## Introduction

Myriad Desktop is a desktop application that provides an easy to use graphical user interface for scanning datasets for Regions of Interest (ROI) built on the [Myriad](https://emphysic.com/myriad/) fault-tolerant data reduction framework.  A video demonstrating the use of Desktop is available from [Emphysic's Wistia page](https://emphysic.wistia.com/medias/sbbft90a67).

The desktop tool employs the [pipeline parallelism methodology](https://emphysic.com/myriad/faq/#concurrency) to parallelize data processing, in which an extensive computation is broken down into a series of independent concurrent steps or stages.  Since each stage operates concurrently, the total throughput of the system (measured in iterations per unit time) is determined by the slowest stage in the pipeline.  

Each stage is configured as a “router” with a configurable number of workers.  When a stage receives a work order, the router adds the work to a shared queue.  The next available worker grabs the next work order from the queue and begins processing.  If the worker successfully completes the work, it sends the results back to the router which then sends the results to the next stage in the processing pipeline.  

A work order is only removed after the work is successfully completed, so if the operation fails during processing the next available worker simply restarts the job.  Since each stage is independent, in the event of failure only the immediate task in the current stage is lost.  No other part of processing is affected.

Stages are designed in Myriad Desktop to work from left to right, e.g. in a standard pipeline data are first read and then sent to the Gaussian Pyramid router, which sends its results to the Sliding Window router when complete, etc.  Each stage is optional so that if Gaussian Pyramid operations are not required for example data can be directly sent to the Sliding Window router instead.

## System Requirements
Trainer will run anywhere where Java 8 is available.  The current minimum requirements for Java 8 on the desktop are summarized below.

* Memory: 128MB RAM
* Disk: 126MB
* Processor: Pentium 2 266MHz or better

Full details on Java 8 requirements are available from [Oracle's Java site](http://www.oracle.com/technetwork/java/javase/certconfig-2095354.html) .  Myriad has been tested to compile and run on Windows 7, Windows 10, various Linux distributions, TrueOS, OpenBSD, and FreeBSD.

## Installation
The [Myriad library](https://gitlab.com/ccoughlin/datareader) must be installed to build Desktop.  Both are distributed as Apache Maven projects, full details on installation are available from the [myriad-docs project](https://gitlab.com/ccoughlin/myriad-docs/blob/master/docs/install.md).

## Quick Start
We've prepared a [bundle](http://myrdocs.azurewebsites.net/myriad_samples.zip) to help you get started.  Inside is a model that has been trained to find indications of structural damage in sensor data and some sample input files to get you started.  Download the bundle and extract the model and the sample files in a convenient location, then use them as you proceed through the [documentation](http://myrdocs.azurewebsites.net/desktop/).

When you're ready to create your own ROI models, have a look at the companion tool [Myriad Trainer](trainer.md).

## Licensing
Copyright 2017 Emphysic, LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.