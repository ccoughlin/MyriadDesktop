/*
 * com.emphysic.myriad.ui.desktop.controllers.RelayActor
 *
 * Copyright (c) 2017 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.emphysic.myriad.ui.desktop.controllers;

import static java.util.concurrent.TimeUnit.SECONDS;
import scala.concurrent.duration.Duration;
import akka.actor.ActorRef;
import akka.actor.ActorIdentity;
import akka.actor.Identify;
import akka.actor.Terminated;
import akka.actor.UntypedActor;
import akka.actor.ReceiveTimeout;
import akka.japi.Procedure;
import lombok.extern.slf4j.Slf4j;

/**
 * RelayActor - looks up remote Actors and relays messages
 * @author chris
 */
@Slf4j
public class RelayActor extends UntypedActor {
  /**
   * Path to remote Actor of the form
   * akka.<protocol>://<actorsystemname>@<hostname>:<port>/<actor path>
   */
  private final String path;
  /**
   * Reference to remote Actor
   */
  private ActorRef remote = null;

  public RelayActor(String path) {
    this.path = path;
    sendIdentifyRequest();
  }

  /**
   * Sends an Identify request to the remote Actor
   */
  private void sendIdentifyRequest() {
    getContext().actorSelection(path).tell(new Identify(path), getSelf());
    getContext()
        .system()
        .scheduler()
        .scheduleOnce(Duration.create(3, SECONDS), getSelf(),
            ReceiveTimeout.getInstance(), getContext().dispatcher(), getSelf());
  }

  @Override
  public void onReceive(Object message) throws Exception {
    if (message instanceof ActorIdentity) {
      remote = ((ActorIdentity) message).getRef();
      if (remote == null) {
        log.info("Remote actor not available: " + path);
      } else {
        getContext().watch(remote);
        getContext().become(active, true);
      }
    } else if (message instanceof ReceiveTimeout) {
      sendIdentifyRequest();
    } else {
      log.info("Not ready yet");
    }
  }

  Procedure<Object> active = new Procedure<Object>() {
    @Override
    public void apply(Object message) {
      if (message instanceof Terminated) {
        log.info("Terminating relay");
        sendIdentifyRequest();
        getContext().unbecome();
      } else if (message instanceof ReceiveTimeout) {
        // ignore
      } else {
        remote.tell(message, getSender());
      }
    }
  };
}