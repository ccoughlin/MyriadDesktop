/*
 * com.emphysic.myriad.ui.desktop.controllers.ListenerActor
 *
 * Copyright (c) 2017 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.emphysic.myriad.ui.desktop.controllers;

import akka.actor.UntypedActor;
import com.emphysic.myriad.core.data.roi.ROI;
import com.emphysic.myriad.network.messages.ImmutableMessage;
import com.emphysic.myriad.network.messages.ROIMessage;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.DefaultListModel;
import lombok.extern.slf4j.Slf4j;

/**
 * ListenerActor - monitors Myriad network for the desktop UI
 * @author chris
 */
@Slf4j
public class ListenerActor extends UntypedActor {
    /**
     * Handle to the JList list of Regions of Interest (ROI) - incoming
     * ROIs are added to this list to dynamically update the JList
     */
    DefaultListModel roiListModel;
    
    /**
     * List of ROIs found for each original source
     */
    Map<String, List<ROI>> rois;
    
    public ListenerActor(DefaultListModel roiList, Map<String, List<ROI>> rois) {
        roiListModel = roiList;
        this.rois = rois;
    }

    @Override
    public void onReceive(Object message) throws Throwable {
        if (message instanceof ROIMessage) {
            log.info(getSelf() + " - received ROIMessage: " + message.toString());
            if (((ROIMessage) message).getROI() != null) {
                ROI roi = ((ROIMessage) message).getROI();
                String metadata = ((ROIMessage) message).getMetadata();
                Map<String, String> md = ImmutableMessage.getMetadata(metadata);
                roi.setMetadata(metadata);
                roiListModel.addElement(roi);
                String src = md.getOrDefault("source", "");
                if (src != null && ! src.isEmpty()) {
                    if (!rois.containsKey(src)) {
                        rois.put(src, new ArrayList<>());
                    }
                    rois.get(src).add(roi);
                }
            }
        }
    }
}
