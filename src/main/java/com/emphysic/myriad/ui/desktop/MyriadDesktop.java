/*
 * com.emphysic.myriad.ui.desktop.MyriadDesktop
 *
 * Copyright (c) 2017 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.emphysic.myriad.ui.desktop;

import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.roi.ROI;
import com.emphysic.myriad.ui.desktop.controllers.MyriadDesktopController;
import com.emphysic.myriad.ui.desktop.controllers.MyriadDesktopController.ROITYPE;
import static com.emphysic.myriad.ui.desktop.controllers.MyriadDesktopController.ROITYPE.EXTERNAL;
import static com.emphysic.myriad.ui.desktop.controllers.MyriadDesktopController.ROITYPE.INTERNAL;
import static com.emphysic.myriad.ui.desktop.controllers.MyriadDesktopController.ROITYPE.REST;
import com.emphysic.myriad.ui.desktop.controllers.ROIResultGenerator;
import com.emphysic.myriad.ui.desktop.dialogs.AboutDialog;
import com.emphysic.myriad.ui.desktop.dialogs.AboutEmphysicDialog;
import com.emphysic.myriad.ui.desktop.dialogs.ClientDialog;
import com.emphysic.myriad.ui.desktop.dialogs.ClientDialog.APIParams;
import com.emphysic.myriad.ui.desktop.dialogs.IProgressDialog;
import com.emphysic.myriad.ui.desktop.dialogs.LicenseDialog;
import com.emphysic.myriad.ui.desktop.dialogs.LogDialog;
import com.emphysic.myriad.ui.desktop.dialogs.PlotDialog;
import com.emphysic.myriad.ui.desktop.dialogs.PropertiesDialog;
import com.emphysic.myriad.ui.desktop.util.WorkerWaiter;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Random;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.ArrayUtils;

/**
 * MyriadDesktop - Swing UI for running a simple Myriad processing pipeline.
 * Configure data ingestion, pyramid, etc. in a "linked list" of Myriad
 * stages.
 * @author chris
 */
@Slf4j
public final class MyriadDesktop extends javax.swing.JFrame {
    /**
     * Controller
     */
    private final MyriadDesktopController ctrl;
    /**
     * Default number of Actors for each stage
     */
    private final int DEFAULTWORKERS = 10;
    /**
     * List of files to be analyzed
     */
    private final DefaultListModel ingestionSrcModel = new DefaultListModel();
    /**
     * Application log display
     */
    private static LogDialog logWindow;
    
    private static final String lastROIprop = "roi.last";
    
    /**
     * Creates new form
     */
    public MyriadDesktop() {
        String propsPath = System.getProperty( "com.emphysic.myriad.desktop.properties");
        if (propsPath == null || propsPath.isEmpty()) {
            propsPath = MyriadDesktopController.getPropertiesFileName();
        }
        ctrl = new MyriadDesktopController(
                this, 
                propsPath
        );
        initComponents();
        additionalInit();
    }
    
    /**
     * Returns the URL for the main app icon
     * @return URL
     */
    public URL getIconURL() {
        return getClass().getResource("/images/myriad-1 icon.png");
    }
    
    /**
     * Returns the icon for the main app
     * @return Icon
     */
    public ImageIcon getIcon() {
        return new ImageIcon(getIconURL());
    }
    
    /**
     * Returns the URL for the splash screen logo
     * @return URL
     */
    public URL getLogoURL() {
        return getClass().getResource("/images/myriad-1.png");
    }
    
    /**
     * Additional Swing initialization code
     */
    public void additionalInit() {
        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                ctrl.shutdown();
                //showLogWindow(false);
                //ctrl.saveProperties();
                System.exit(0);
            }
        });
        logWindow = new LogDialog(this, false);
        setIconImage(getIcon().getImage());
        logWindow.setIconImage(getIcon().getImage());
        preprocOpCB.setModel(new DefaultComboBoxModel(ctrl.getAvailableOperations()));
        windowOpCB.setModel(new DefaultComboBoxModel(ctrl.getAvailableOperations()));
        roiTypeCB.setModel(new DefaultComboBoxModel(ctrl.getAvailableROIFinders()));
        String roiType = ctrl.getProperty("roi.type");
        if (roiType != null && !roiType.isEmpty()) {
            int idx = ArrayUtils.indexOf(ctrl.getAvailableROIFinders(), 
                    ctrl.getProperty("roi.type"));
            if (idx != -1) {
                roiTypeCB.setSelectedIndex(idx);
            }
        }
        ingestionSrcLB.setModel(ingestionSrcModel);
        ingestionSrcLB.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {            
                if (evt.getClickCount() == 2) {
                    if (!ingestionSrcLB.isSelectionEmpty()) {
                        PlotDialog plot = new PlotDialog(
                                (JFrame) SwingUtilities.getWindowAncestor(ingestionSrcLB), 
                                false);
                        plot.setTitle(new File(ingestionSrcLB.getSelectedValue()).getName());
                        plot.setIconImage(getIconImage());
                        plot.plot(ingestionSrcLB.getSelectedValue());
                        plot.setVisible(true);
                    }
                }
            }
        });
        remoteIngestionCB.addActionListener((ActionEvent e) -> {
            boolean enabled = !remoteIngestionCB.isSelected();
            nIngestionTF.setEnabled(enabled);
            addIngestionSrcBtn.setEnabled(enabled);
            delIngestionSrcBtn.setEnabled(enabled);
            ingestionSrcLB.setEnabled(enabled);
        });
        roiLB.setModel(ctrl.getROIListModel());
        roiLB.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {            
                if (evt.getClickCount() == 2) {
                    if (!roiLB.isSelectionEmpty()) {
                        Dataset toPlot = roiLB.getSelectedValue().getDataset();
                        try {
                            PlotDialog plot = new PlotDialog(
                                    (JFrame) SwingUtilities.getWindowAncestor(roiLB), 
                                    false);
                            plot.setTitle(roiLB.getSelectedValue().getMetadata());
                            plot.setIconImage(getIconImage());
                            plot.plot(toPlot);
                            plot.setVisible(true);
                        } catch (Exception e) {
                            // Dataset has invalid dims e.g. if preprocessor forces dim change
                            JOptionPane.showMessageDialog(
                                    (JFrame) SwingUtilities.getWindowAncestor(roiLB),
                                    "Unable to plot specified Dataset, error was: " + e.getMessage(),
                                    "Unable To Plot Dataset",
                                    JOptionPane.OK_OPTION);
                        }
                    }
                }
            }
        });
        roiFileTF.setText(ctrl.getProperty(lastROIprop));
        resultsLB.setModel(ctrl.getResultsListModel());
        resultsLB.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {            
                if (evt.getClickCount() == 2) {
                    if (!resultsLB.isSelectionEmpty()) {
                        Object[] possibleValues = ROIResultGenerator.REPTYPE.values();
                        ROIResultGenerator.REPTYPE selectedValue = (ROIResultGenerator.REPTYPE)JOptionPane.showInputDialog(
                                (JFrame) SwingUtilities.getWindowAncestor(resultsLB),
                                "Please choose a method for displaying the ROI.", 
                                "Choose ROI Display",
                                JOptionPane.QUESTION_MESSAGE, 
                                null,
                                possibleValues, 
                                possibleValues[0]);
                        if (selectedValue != null) {
                            Dataset d = ctrl.getResultDataset(resultsLB.getSelectedValue(), 
                                    selectedValue);
                            if (d!= null) {
                                PlotDialog plot = new PlotDialog(
                                        (JFrame) SwingUtilities.getWindowAncestor(resultsLB), 
                                        false);
                                plot.setTitle(resultsLB.getSelectedValue());
                                plot.setIconImage(getIconImage());
                                plot.plot(d);
                                plot.setVisible(true);
                            }
                        }
                    }
                }
            }
        });
        String showLog = ctrl.getProperty("desktop.log.show");        
        if (showLog!=null && !showLog.isEmpty()) {
            boolean show = Boolean.valueOf(showLog);
            startLogCB.setSelected(show);
            showLogWindow(show);
        }
    }
    
    /**
     * Retrieves the handle to the JTextPane used to display the app log
     * @return handle to log pane
     */
    public static JTextPane getLogPane() {
        return LogDialog.logPane;
    }
    
    /**
     * Convenience method for returning a number from a String or a default
     * value if the conversion fails.
     * @param field String to read
     * @param fallbackValue default value
     * @return number read from the field, or the default value if conversion fails
     */
    public int getInt(String field, int fallbackValue) {
        try {
            return Integer.parseInt(field);
        } catch (Exception e) {
            return fallbackValue;
        }
    }
    
    /**
     * Reports whether the Normalize Data option is selected.
     * @return true if data should be normalized
     */
    public boolean normalizeData() {
        return normDataCB.isSelected();
    }
    
    /**
     * Specify whether data should be normalized or not
     * @param normalize true if data should be normalized
     */
    public void normalizeData(boolean normalize) {
        normDataCB.setSelected(normalize);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel7 = new javax.swing.JPanel();
        resultsPanel = new javax.swing.JTabbedPane();
        ingestionPanel = new javax.swing.JPanel();
        remoteIngestionCB = new javax.swing.JCheckBox();
        ingestionPathTF = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        nIngestionTF = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        ingestionSrcLB = new javax.swing.JList<>();
        addIngestionSrcBtn = new javax.swing.JButton();
        delIngestionSrcBtn = new javax.swing.JButton();
        udIngBtn = new javax.swing.JButton();
        normDataCB = new javax.swing.JCheckBox();
        pyramidPanel = new javax.swing.JPanel();
        remotePyramidCB = new javax.swing.JCheckBox();
        pyramidPathTF = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        nPyramidTF = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        pyramidRadiusTF = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        pyramidScalingTF = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        pyramidCutoffTF = new javax.swing.JTextField();
        udPyrBtn = new javax.swing.JButton();
        preprocPanel = new javax.swing.JPanel();
        remotePreProcCB = new javax.swing.JCheckBox();
        preprocPathTF = new javax.swing.JTextField();
        preprocOpCB = new javax.swing.JComboBox<>();
        configOpBtn = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        nPreProcTF = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        udPreProcBtn = new javax.swing.JButton();
        slidingWindowPanel = new javax.swing.JPanel();
        remoteWindowCB = new javax.swing.JCheckBox();
        windowPathTF = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        nWindowTF = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        windowStepTF = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        windowWidthTF = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        windowOpCB = new javax.swing.JComboBox<>();
        configWinOpBtn = new javax.swing.JButton();
        udWinBtn = new javax.swing.JButton();
        jLabel15 = new javax.swing.JLabel();
        windowHeightTF = new javax.swing.JTextField();
        roiPanel = new javax.swing.JPanel();
        remoteROICB = new javax.swing.JCheckBox();
        roiPathTF = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        nROITF = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        roiTypeCB = new javax.swing.JComboBox<>();
        roiFileTF = new javax.swing.JTextField();
        chooseROIBtn = new javax.swing.JButton();
        udROIBtn = new javax.swing.JButton();
        reportingPanel = new javax.swing.JPanel();
        remoteReporterCB = new javax.swing.JCheckBox();
        reporterPathTF = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        nReportersTF = new javax.swing.JTextField();
        udRptBtn = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        roiLB = new javax.swing.JList<ROI>();
        clrReportsBtn = new javax.swing.JButton();
        saveReportsBtn = new javax.swing.JButton();
        resultsPane = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jScrollPane4 = new javax.swing.JScrollPane();
        resultsLB = new javax.swing.JList<>();
        saveResultsBtn = new javax.swing.JButton();
        clrResultsBtn = new javax.swing.JButton();
        getResultsBtn = new javax.swing.JButton();
        updatePipelineBtn = new javax.swing.JButton();
        runMyriadButton = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        fileMnu = new javax.swing.JMenu();
        exitMenuItem = new javax.swing.JMenuItem();
        viewMnu = new javax.swing.JMenu();
        showLogMenuItem = new javax.swing.JMenuItem();
        startLogCB = new javax.swing.JCheckBoxMenuItem();
        helpMnu = new javax.swing.JMenu();
        mnuProps = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        opendocsMenuItem = new javax.swing.JMenuItem();
        licenseMenuItem = new javax.swing.JMenuItem();
        aboutMenuItem = new javax.swing.JMenuItem();
        emphysicMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Myriad Desktop");

        remoteIngestionCB.setText("Remote");
        remoteIngestionCB.setToolTipText("Check if ingestion is done remotely");

        ingestionPathTF.setToolTipText("Path to Data Ingestion Pool");

        jLabel12.setText("Number of Workers");

        nIngestionTF.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        nIngestionTF.setText("10");
        nIngestionTF.setToolTipText("Number of workers in the data ingestion pool");

        ingestionSrcLB.setToolTipText("File(s) to be analyzed");
        jScrollPane1.setViewportView(ingestionSrcLB);

        addIngestionSrcBtn.setText("+");
        addIngestionSrcBtn.setToolTipText("Add one or more files for analysis");
        addIngestionSrcBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addIngestionSrcBtnActionPerformed(evt);
            }
        });

        delIngestionSrcBtn.setText("-");
        delIngestionSrcBtn.setToolTipText("Remove selected file(s) from analysis");
        delIngestionSrcBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                delIngestionSrcBtnActionPerformed(evt);
            }
        });

        udIngBtn.setText("Update Stage");
        udIngBtn.setToolTipText("Update this stage with current settings");
        udIngBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                udIngBtnActionPerformed(evt);
            }
        });

        normDataCB.setText("Normalize Data?");
        normDataCB.setToolTipText("If checked, data are normalized between 0 and 1");

        javax.swing.GroupLayout ingestionPanelLayout = new javax.swing.GroupLayout(ingestionPanel);
        ingestionPanel.setLayout(ingestionPanelLayout);
        ingestionPanelLayout.setHorizontalGroup(
            ingestionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ingestionPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(ingestionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ingestionPanelLayout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 842, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(ingestionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(delIngestionSrcBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(addIngestionSrcBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE))
                        .addContainerGap())
                    .addGroup(ingestionPanelLayout.createSequentialGroup()
                        .addGroup(ingestionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(ingestionPanelLayout.createSequentialGroup()
                                .addComponent(remoteIngestionCB)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(ingestionPathTF))
                            .addGroup(ingestionPanelLayout.createSequentialGroup()
                                .addComponent(jLabel12)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(nIngestionTF, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addGap(10, 10, 10))
                    .addGroup(ingestionPanelLayout.createSequentialGroup()
                        .addGroup(ingestionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(udIngBtn)
                            .addComponent(normDataCB))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        ingestionPanelLayout.setVerticalGroup(
            ingestionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ingestionPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(ingestionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(remoteIngestionCB)
                    .addComponent(ingestionPathTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(ingestionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(nIngestionTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(normDataCB)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(ingestionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 211, Short.MAX_VALUE)
                    .addGroup(ingestionPanelLayout.createSequentialGroup()
                        .addComponent(addIngestionSrcBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(delIngestionSrcBtn)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(udIngBtn)
                .addContainerGap())
        );

        resultsPanel.addTab("Data", ingestionPanel);

        remotePyramidCB.setText("Remote");
        remotePyramidCB.setToolTipText("Check if scaling is done remotely");

        pyramidPathTF.setToolTipText("Path to Scaling Pool");

        jLabel1.setText("Number of Workers");

        nPyramidTF.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        nPyramidTF.setText("10");
        nPyramidTF.setToolTipText("Number of workers in the pool");

        jLabel2.setText("Blur Radius");

        pyramidRadiusTF.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        pyramidRadiusTF.setText("5");
        pyramidRadiusTF.setToolTipText("Radius of blur operation");

        jLabel3.setText("Scaling Factor");

        pyramidScalingTF.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        pyramidScalingTF.setText("2");
        pyramidScalingTF.setToolTipText("Current step size / next step size, typically 2");

        jLabel4.setText("Cutoff Size");

        pyramidCutoffTF.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        pyramidCutoffTF.setText("1");
        pyramidCutoffTF.setToolTipText("Stop when any of step's dimensions this size or smaller");

        udPyrBtn.setText("Update Stage");
        udPyrBtn.setToolTipText("Updates processing stage with current settings");
        udPyrBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                udPyrBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pyramidPanelLayout = new javax.swing.GroupLayout(pyramidPanel);
        pyramidPanel.setLayout(pyramidPanelLayout);
        pyramidPanelLayout.setHorizontalGroup(
            pyramidPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pyramidPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pyramidPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pyramidPanelLayout.createSequentialGroup()
                        .addComponent(remotePyramidCB)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(pyramidPathTF))
                    .addGroup(pyramidPanelLayout.createSequentialGroup()
                        .addGroup(pyramidPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pyramidPanelLayout.createSequentialGroup()
                                .addGroup(pyramidPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel4))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(pyramidPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(pyramidRadiusTF)
                                    .addComponent(nPyramidTF, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
                                    .addComponent(pyramidScalingTF)
                                    .addComponent(pyramidCutoffTF)))
                            .addComponent(udPyrBtn))
                        .addGap(0, 747, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pyramidPanelLayout.setVerticalGroup(
            pyramidPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pyramidPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pyramidPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(remotePyramidCB)
                    .addComponent(pyramidPathTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pyramidPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(nPyramidTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pyramidPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(pyramidRadiusTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pyramidPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(pyramidScalingTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pyramidPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(pyramidCutoffTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 161, Short.MAX_VALUE)
                .addComponent(udPyrBtn)
                .addContainerGap())
        );

        resultsPanel.addTab("Scale Space", pyramidPanel);

        remotePreProcCB.setText("Remote");
        remotePreProcCB.setToolTipText("Check if preprocessing is done remotely");

        preprocPathTF.setToolTipText("Path to Preprocessing Pool");

        preprocOpCB.setToolTipText("Pre-processing operation performed on entire dataset");

        configOpBtn.setText("Configure...");
        configOpBtn.setToolTipText("Set configuration for current preprocessing operation");
        configOpBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                configOpBtnActionPerformed(evt);
            }
        });

        jLabel10.setText("Number of Workers");

        nPreProcTF.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        nPreProcTF.setText("10");
        nPreProcTF.setToolTipText("Number of workers in the preprocessing pool");

        jLabel11.setText("Operation");

        udPreProcBtn.setText("Update Stage");
        udPreProcBtn.setToolTipText("Updates processing stage with current settings");
        udPreProcBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                udPreProcBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout preprocPanelLayout = new javax.swing.GroupLayout(preprocPanel);
        preprocPanel.setLayout(preprocPanelLayout);
        preprocPanelLayout.setHorizontalGroup(
            preprocPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(preprocPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(preprocPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(preprocPanelLayout.createSequentialGroup()
                        .addComponent(remotePreProcCB)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(preprocPathTF))
                    .addGroup(preprocPanelLayout.createSequentialGroup()
                        .addGroup(preprocPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel11)
                            .addComponent(udPreProcBtn)
                            .addGroup(preprocPanelLayout.createSequentialGroup()
                                .addGroup(preprocPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(preprocOpCB, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, preprocPanelLayout.createSequentialGroup()
                                        .addComponent(jLabel10)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(nPreProcTF, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(configOpBtn)))
                        .addGap(0, 650, Short.MAX_VALUE)))
                .addContainerGap())
        );
        preprocPanelLayout.setVerticalGroup(
            preprocPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(preprocPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(preprocPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(remotePreProcCB)
                    .addComponent(preprocPathTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(preprocPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(nPreProcTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(preprocPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(preprocOpCB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(configOpBtn))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 188, Short.MAX_VALUE)
                .addComponent(udPreProcBtn)
                .addContainerGap())
        );

        resultsPanel.addTab("Data Preprocessing", preprocPanel);

        remoteWindowCB.setText("Remote");
        remoteWindowCB.setToolTipText("Select if Sliding Window is performed remotely");

        windowPathTF.setToolTipText("Path to Sliding Window Pool");

        jLabel5.setText("Number of Workers");

        nWindowTF.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        nWindowTF.setText("10");
        nWindowTF.setToolTipText("Number of workers in the pool");

        jLabel6.setText("Step Size");

        windowStepTF.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        windowStepTF.setText("5");
        windowStepTF.setToolTipText("Move N points between windows");

        jLabel7.setText("Window Size");

        windowWidthTF.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        windowWidthTF.setText("15");
        windowWidthTF.setToolTipText("Window is WxH points");

        jLabel13.setText("Window Processing Operation");

        windowOpCB.setToolTipText("[Optional] Perform preprocessing on each window");

        configWinOpBtn.setText("Configure...");
        configWinOpBtn.setToolTipText("Configure window processing operation");
        configWinOpBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                configWinOpBtnActionPerformed(evt);
            }
        });

        udWinBtn.setText("Update Stage");
        udWinBtn.setToolTipText("Updates processing stage with current settings");
        udWinBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                udWinBtnActionPerformed(evt);
            }
        });

        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel15.setText("x");

        windowHeightTF.setText("15");
        windowHeightTF.setToolTipText("Window is WxH points");

        javax.swing.GroupLayout slidingWindowPanelLayout = new javax.swing.GroupLayout(slidingWindowPanel);
        slidingWindowPanel.setLayout(slidingWindowPanelLayout);
        slidingWindowPanelLayout.setHorizontalGroup(
            slidingWindowPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(slidingWindowPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(slidingWindowPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(slidingWindowPanelLayout.createSequentialGroup()
                        .addComponent(remoteWindowCB)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(windowPathTF))
                    .addGroup(slidingWindowPanelLayout.createSequentialGroup()
                        .addGroup(slidingWindowPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(slidingWindowPanelLayout.createSequentialGroup()
                                .addGroup(slidingWindowPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, 169, Short.MAX_VALUE)
                                    .addComponent(windowOpCB, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(configWinOpBtn))
                            .addComponent(udWinBtn)
                            .addGroup(slidingWindowPanelLayout.createSequentialGroup()
                                .addGroup(slidingWindowPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel7))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(slidingWindowPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(windowWidthTF, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(windowStepTF, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
                                    .addComponent(nWindowTF))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(windowHeightTF, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 624, Short.MAX_VALUE)))
                .addContainerGap())
        );
        slidingWindowPanelLayout.setVerticalGroup(
            slidingWindowPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(slidingWindowPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(slidingWindowPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(remoteWindowCB)
                    .addComponent(windowPathTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(slidingWindowPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(nWindowTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(slidingWindowPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(windowStepTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(slidingWindowPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(windowWidthTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15)
                    .addComponent(windowHeightTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(slidingWindowPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(windowOpCB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(configWinOpBtn))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 126, Short.MAX_VALUE)
                .addComponent(udWinBtn)
                .addContainerGap())
        );

        resultsPanel.addTab("Sliding Window", slidingWindowPanel);

        remoteROICB.setText("Remote");
        remoteROICB.setToolTipText("Check if ROI detection is performed remotely");

        roiPathTF.setToolTipText("Path to ROI detector pool");

        jLabel8.setText("Number of Workers");

        nROITF.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        nROITF.setText("10");
        nROITF.setToolTipText("Number of workers in the pool");

        jLabel9.setText("ROI Detector");

        roiTypeCB.setToolTipText("Choose type of ROI detector");

        roiFileTF.setToolTipText("Path to ROI Detector");

        chooseROIBtn.setText("Choose...");
        chooseROIBtn.setToolTipText("Browse to Myriad model / external application");
        chooseROIBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chooseROIBtnActionPerformed(evt);
            }
        });

        udROIBtn.setText("Update Stage");
        udROIBtn.setToolTipText("Updates processing stage with current settings");
        udROIBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                udROIBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout roiPanelLayout = new javax.swing.GroupLayout(roiPanel);
        roiPanel.setLayout(roiPanelLayout);
        roiPanelLayout.setHorizontalGroup(
            roiPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(roiPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(roiPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(roiPanelLayout.createSequentialGroup()
                        .addComponent(remoteROICB)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(roiPathTF))
                    .addGroup(roiPanelLayout.createSequentialGroup()
                        .addComponent(roiTypeCB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(chooseROIBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(roiFileTF, javax.swing.GroupLayout.DEFAULT_SIZE, 761, Short.MAX_VALUE))
                    .addGroup(roiPanelLayout.createSequentialGroup()
                        .addGroup(roiPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(roiPanelLayout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(nROITF, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel9)
                            .addComponent(udROIBtn))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        roiPanelLayout.setVerticalGroup(
            roiPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(roiPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(roiPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(remoteROICB)
                    .addComponent(roiPathTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(roiPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(nROITF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(roiPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(roiTypeCB, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chooseROIBtn)
                    .addComponent(roiFileTF, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 193, Short.MAX_VALUE)
                .addComponent(udROIBtn)
                .addContainerGap())
        );

        resultsPanel.addTab("Region Of Interest Detection", roiPanel);

        remoteReporterCB.setText("Remote");
        remoteReporterCB.setToolTipText("Check if ROI Reporting is performed remotely");

        reporterPathTF.setToolTipText("Path to ROI Reporter Pool");

        jLabel14.setText("Number of Workers");

        nReportersTF.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        nReportersTF.setText("10");
        nReportersTF.setToolTipText("Number of workers in the pool");

        udRptBtn.setText("Update Stage");
        udRptBtn.setToolTipText("Updates processing stage with current settings");
        udRptBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                udRptBtnActionPerformed(evt);
            }
        });

        jScrollPane2.setViewportView(roiLB);

        clrReportsBtn.setText("Clear");
        clrReportsBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clrReportsBtnActionPerformed(evt);
            }
        });

        saveReportsBtn.setText("Save");
        saveReportsBtn.setToolTipText("Export ROI as list");
        saveReportsBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveReportsBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout reportingPanelLayout = new javax.swing.GroupLayout(reportingPanel);
        reportingPanel.setLayout(reportingPanelLayout);
        reportingPanelLayout.setHorizontalGroup(
            reportingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(reportingPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(reportingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 890, Short.MAX_VALUE)
                    .addGroup(reportingPanelLayout.createSequentialGroup()
                        .addComponent(remoteReporterCB)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(reporterPathTF))
                    .addGroup(reportingPanelLayout.createSequentialGroup()
                        .addGroup(reportingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(udRptBtn)
                            .addGroup(reportingPanelLayout.createSequentialGroup()
                                .addComponent(jLabel14)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(nReportersTF, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(reportingPanelLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(clrReportsBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(saveReportsBtn)))
                .addContainerGap())
        );
        reportingPanelLayout.setVerticalGroup(
            reportingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(reportingPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(reportingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(remoteReporterCB)
                    .addComponent(reporterPathTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(reportingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(nReportersTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 210, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(reportingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(saveReportsBtn)
                    .addComponent(clrReportsBtn))
                .addGap(4, 4, 4)
                .addComponent(udRptBtn)
                .addContainerGap())
        );

        resultsPanel.addTab("Reporting", reportingPanel);

        jScrollPane4.setViewportView(resultsLB);

        jScrollPane3.setViewportView(jScrollPane4);

        saveResultsBtn.setText("Save");
        saveResultsBtn.setToolTipText("Compile results and export datasets");
        saveResultsBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveResultsBtnActionPerformed(evt);
            }
        });

        clrResultsBtn.setText("Clear");
        clrResultsBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clrResultsBtnActionPerformed(evt);
            }
        });

        getResultsBtn.setText("Compile Results");
        getResultsBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                getResultsBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout resultsPaneLayout = new javax.swing.GroupLayout(resultsPane);
        resultsPane.setLayout(resultsPaneLayout);
        resultsPaneLayout.setHorizontalGroup(
            resultsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(resultsPaneLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(resultsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 890, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, resultsPaneLayout.createSequentialGroup()
                        .addComponent(getResultsBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(clrResultsBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(saveResultsBtn)))
                .addContainerGap())
        );
        resultsPaneLayout.setVerticalGroup(
            resultsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(resultsPaneLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 299, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(resultsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(saveResultsBtn)
                    .addComponent(clrResultsBtn)
                    .addComponent(getResultsBtn))
                .addContainerGap())
        );

        resultsPanel.addTab("Results", resultsPane);

        updatePipelineBtn.setText("Update Pipeline");
        updatePipelineBtn.setToolTipText("Configures entire pipeline with current settings");
        updatePipelineBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updatePipelineBtnActionPerformed(evt);
            }
        });

        runMyriadButton.setText("Run!");
        runMyriadButton.setToolTipText("Starts Myriad processing ");
        runMyriadButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                runMyriadButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(resultsPanel)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(updatePipelineBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(runMyriadButton)))
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(resultsPanel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(updatePipelineBtn)
                    .addComponent(runMyriadButton))
                .addContainerGap())
        );

        fileMnu.setText("File");

        exitMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_X, java.awt.event.InputEvent.ALT_MASK));
        exitMenuItem.setText("Exit");
        exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitMenuItemActionPerformed(evt);
            }
        });
        fileMnu.add(exitMenuItem);

        jMenuBar1.add(fileMnu);

        viewMnu.setText("View");

        showLogMenuItem.setText("Show Log Window");
        showLogMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showLogMenuItemActionPerformed(evt);
            }
        });
        viewMnu.add(showLogMenuItem);

        startLogCB.setText("Show Log on Startup");
        startLogCB.setToolTipText("Toggle automatically opening the log window at startup");
        startLogCB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startLogCBActionPerformed(evt);
            }
        });
        viewMnu.add(startLogCB);

        jMenuBar1.add(viewMnu);

        helpMnu.setText("Help");

        mnuProps.setText("Configuration...");
        mnuProps.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuPropsActionPerformed(evt);
            }
        });
        helpMnu.add(mnuProps);
        helpMnu.add(jSeparator1);

        opendocsMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        opendocsMenuItem.setText("View Online Documentation");
        opendocsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opendocsMenuItemActionPerformed(evt);
            }
        });
        helpMnu.add(opendocsMenuItem);

        licenseMenuItem.setText("License Information");
        licenseMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                licenseMenuItemActionPerformed(evt);
            }
        });
        helpMnu.add(licenseMenuItem);

        aboutMenuItem.setText("About This Program");
        aboutMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aboutMenuItemActionPerformed(evt);
            }
        });
        helpMnu.add(aboutMenuItem);

        emphysicMenuItem.setText("About Emphysic");
        emphysicMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                emphysicMenuItemActionPerformed(evt);
            }
        });
        helpMnu.add(emphysicMenuItem);

        jMenuBar1.add(helpMnu);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Handles request to update the entire pipeline.
     * @param evt 
     */
    private void updatePipelineBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updatePipelineBtnActionPerformed
        String title;
        String msg;
        int dlgType;
        boolean success = updateIngestionStage() &&
                updatePyramidStage() &&
                updatePreProcessingStage() &&
                updateWindowStage() &&
                updateROIDetectionStage() &&
                updateReportingStage();
        if (!success) {
            title = "Unable To Configure Pipeline";
            msg = "One or more errors encountered configuring processing pipeline.";
            dlgType = JOptionPane.ERROR_MESSAGE;
        } else {
            title = "Pipeline Successfully Configured";
            msg = "The processing pipeline was successfully updated.";
            dlgType = JOptionPane.INFORMATION_MESSAGE;
        }
        JOptionPane.showMessageDialog(this, msg, title, dlgType);
    }//GEN-LAST:event_updatePipelineBtnActionPerformed

    /**
     * Handles request to configure a per-window DatasetOperation.
     * @param evt 
     */
    private void configWinOpBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_configWinOpBtnActionPerformed
        ctrl.setWindowOperation((String)windowOpCB.getSelectedItem());
    }//GEN-LAST:event_configWinOpBtnActionPerformed

    /**
     * Handles request to set the internal/external ROI detector.
     * @param evt 
     */
    private void chooseROIBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chooseROIBtnActionPerformed
        String roiApp = null;
        String[] appArgs = null;
        JFileChooser fileChooser = new JFileChooser();
        File roiFile;
        String appSelection = ((String)roiTypeCB.getSelectedItem()).toLowerCase();
        ROITYPE appType = getROITYPE(appSelection);
        switch (appSelection) {
            case "internal":
                fileChooser.setDialogTitle("Please select the folder containing the model files.");
                fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
                FileFilter filter = new FileNameExtensionFilter("Myriad ROI Models","myr");
                fileChooser.addChoosableFileFilter(filter);
                fileChooser.setFileFilter(filter);
                if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
                    roiFile = fileChooser.getSelectedFile();
                    roiApp = roiFile.getAbsolutePath();
                }
                break;
            case "external":
                fileChooser.setDialogTitle("Please select the external ROI application.");
                fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
                fileChooser.setAcceptAllFileFilterUsed(true);
                if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
                    roiFile = fileChooser.getSelectedFile();
                    roiApp = roiFile.getAbsolutePath();
                    String args = getApplicationArguments();
                    if (args != null && !args.isEmpty()) {
                        appArgs = args.split("\\s");
                    }
                }
                break;
            case "rest":
                ClientDialog dlg = new ClientDialog(this, true);
                dlg.setVisible(true);
                APIParams params = dlg.getParams();
                if (params != null) {
                    roiApp = params.url;
                    if (!params.username.isEmpty() && ! params.password.isEmpty()) {
                        appArgs = new String[2];
                        appArgs[0] = params.username;
                        appArgs[1] = params.password;
                    }
                }
                break;
            case "matlab":
                String mpath = ctrl.getProperty("matlab.path");
                if (mpath == null) {
                    mpath = getInterpreterPath();
                    ctrl.setProperty("matlab.path", mpath);
                }
                fileChooser.setDialogTitle("Please select the MATLAB script to run.");
                fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
                if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
                    roiFile = fileChooser.getSelectedFile();
                    String p;
                    if (mpath != null) {
                        p = mpath;
                    } else {
                        p = roiFile.getAbsolutePath();
                    }
                    roiApp = p;
                    StringBuilder argsBuilder = new StringBuilder(" -nodisplay -nosplash -nodesktop -r \"run('");
                    // MATLAB format: set args first, then script to run
                    String args = getApplicationArguments();
                    if (args != null && ! args.isEmpty()) {
                        argsBuilder.append(args).append(";");
                    }
                    argsBuilder.append(roiFile.getAbsolutePath()).append("');exit;\"");
                    appArgs = argsBuilder.toString().split("\\s");
                }
                break;
            case "python":
                String ppath = ctrl.getProperty("python.path");
                if (ppath == null) {
                    ppath = getInterpreterPath();
                    ctrl.setProperty("python.path", ppath);
                }
                fileChooser.setDialogTitle("Please select the Python script to run.");
                fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
                if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
                    roiFile = fileChooser.getSelectedFile();
                    String p;
                    if (ppath != null) {
                        p = ppath + " " + roiFile.getAbsolutePath();
                    } else {
                        p = roiFile.getAbsolutePath();
                    }
                    roiApp = p;
                    String args = getApplicationArguments();
                    if (args != null && !args.isEmpty()) {
                        appArgs = args.split("\\s");
                    }
                }
                break;
            default:
                break;
        }
        if (appType != null) {
            setROI(appType, roiApp, appArgs);
            ctrl.setProperty("roi.type", appSelection);
        }
    }//GEN-LAST:event_chooseROIBtnActionPerformed

    /**
     * Prompt the user to supply any command line arguments required for an ROI finder.
     * @return user's input
     */
    private String getApplicationArguments() {
        return JOptionPane.showInputDialog(this,
                "Enter any required command line arguments for this application.\nLeave empty for none.",
                "Configure Command Line Arguments",
                JOptionPane.OK_OPTION);
    }
    
    private void setROI(ROITYPE appType, String roiApp, String[] args) {
        String roi = ctrl.getROIFinder(appType, roiApp, args);
        roiFileTF.setText(roi);
        ctrl.setProperty(lastROIprop, roi);
    }
    
    private ROITYPE getROITYPE(String appSelection) {
        ROITYPE appType = null;
        switch (appSelection) {
            case "internal":
                appType = INTERNAL;
                break;
            case "rest":
                appType = REST;
                break;
            case "external":
            case "matlab":
            case "python":
                appType = EXTERNAL;
                break;
        }
        return appType;
    }
    
    /**
     * Handles request to run the pipeline - every input file is ingested and
     * sent through the processing pipeline.
     * @param evt 
     */
    private void runMyriadButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_runMyriadButtonActionPerformed
        IProgressDialog dlg = new IProgressDialog(this, false);
        dlg.setTitle("Executing Pipeline");
        dlg.setMessage("Please wait, initializing pipeline...");
        SwingWorker worker = new SwingWorker<Void, Void>() {
            @Override
            public Void doInBackground() {
                setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR));
                ctrl.start();
                if (!remoteIngestionCB.isSelected()) {
                    for (int i = 0; i < ingestionSrcModel.size(); i++) {
                        String nextFile = (String) ingestionSrcModel.get(i);
                        dlg.setMessage("Ingesting " + nextFile + "...");
                        ctrl.ingestInputFile(nextFile);
                    }
                }
                setCursor(java.awt.Cursor.getDefaultCursor());
                return null;
            }
        };        
        dlg.setFuture(worker);
        dlg.setVisible(true);
        worker.addPropertyChangeListener(new WorkerWaiter(dlg));
        worker.execute();        
    }//GEN-LAST:event_runMyriadButtonActionPerformed

    /**
     * Handles request to add one or more data files.
     * @param evt 
     */
    private void addIngestionSrcBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addIngestionSrcBtnActionPerformed
        JFileChooser fc = new JFileChooser();
        fc.setLocation(getLocation());
        fc.setDialogTitle("Please select one or more files to analyze.");
        fc.setDialogType(JFileChooser.OPEN_DIALOG);
        fc.setMultiSelectionEnabled(true);
        fc.setAcceptAllFileFilterUsed(true);
        if (fc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            for (File selectedFile: fc.getSelectedFiles()) {
                String source = null;
                if (selectedFile.isDirectory()) {
                    for (File f: selectedFile.listFiles()) {
                        source = f.getAbsolutePath();
                    }
                } else {
                    source = selectedFile.getAbsolutePath();
                }
                if (source != null && !ingestionSrcModel.contains(source)) {
                    ingestionSrcModel.addElement(source);
                }
            }
        }
    }//GEN-LAST:event_addIngestionSrcBtnActionPerformed

    /**
     * Handles request to display the Log Window.
     * @param evt 
     */
    private void showLogMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showLogMenuItemActionPerformed
        showLogWindow(true);
    }//GEN-LAST:event_showLogMenuItemActionPerformed

    /**
     * Handles request to update the data ingestion stage.
     * @param evt 
     */
    private void udIngBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_udIngBtnActionPerformed
        updateIngestionStage();
    }//GEN-LAST:event_udIngBtnActionPerformed

    /**
     * Updates the Data Ingestion stage with current settings.
     * @return true if successful false otherwise
     */
    public boolean updateIngestionStage() {
        try {
            String ingestorPath = ctrl.setIngestorActor(
                getInt(nIngestionTF.getText(), DEFAULTWORKERS), 
                ingestionPathTF.getText(), 
                remoteIngestionCB.isSelected(),
                normDataCB.isSelected());
            ingestionPathTF.setText(ingestorPath);
            return true;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this,
                    "Error encountered updating data stage:\n" + e.getMessage(),
                    "Unable To Update Data Stage",
                    JOptionPane.ERROR_MESSAGE);
            log.error(e.getMessage());
        }
        return false;
    }
    
    /**
     * Handles request to update the scaling (aka Pyramid) stage
     * @param evt 
     */
    private void udPyrBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_udPyrBtnActionPerformed
        updatePyramidStage();
    }//GEN-LAST:event_udPyrBtnActionPerformed

    /**
     * Updates the space scaling (aka Pyramid) processing stage with current
     * settings.
     * @return true if successful false otherwise
     */
    public boolean updatePyramidStage() {
        try {
            String pyramidPath = ctrl.setPyramidActor(
                    getInt(nPyramidTF.getText(), DEFAULTWORKERS), 
                    pyramidPathTF.getText(), 
                    remotePyramidCB.isSelected(), 
                    getInt(pyramidRadiusTF.getText(), 5), 
                    getInt(pyramidScalingTF.getText(), 2), 
                    getInt(pyramidCutoffTF.getText(), 1));
            pyramidPathTF.setText(pyramidPath);
            return true;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this,
                    "Error encountered updating scale space stage:\n" + e.getMessage(),
                    "Unable To Update Scale Space Stage",
                    JOptionPane.ERROR_MESSAGE);
            log.error(e.getMessage());
        }
        return false;
    }
    
    /**
     * Updates the pre-processing stage with current settings.
     * @return true if successful false otherwise
     */
    public boolean updatePreProcessingStage() {
        try {
            String preprocOp = (String)preprocOpCB.getSelectedItem();
            if (preprocOp != null && ! preprocOp.isEmpty()) {
                ctrl.setPreProcessingOperation(preprocOp);
            }
            String preprocPath = ctrl.setPreProcessorActor(
                    getInt(nPreProcTF.getText(), DEFAULTWORKERS), 
                    preprocPathTF.getText(), 
                    remotePreProcCB.isSelected());
            preprocPathTF.setText(preprocPath);
            return true;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this,
                    "Error encountered updating preprocessing stage:\n" + e.getMessage(),
                    "Unable To Update Preprocessing Stage",
                    JOptionPane.ERROR_MESSAGE);
            log.error(e.getMessage());
        }
        return false;
    }
    
    /**
     * Handles request to update the sliding window stage.
     * @param evt 
     */
    private void udWinBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_udWinBtnActionPerformed
        updateWindowStage();
    }//GEN-LAST:event_udWinBtnActionPerformed

    /**
     * Updates the sliding window stage with current settings.
     * @return true if successful false otherwise
     */
    public boolean updateWindowStage() {
        try {
            String windowOp = (String)windowOpCB.getSelectedItem();
            if (windowOp != null && !windowOp.isEmpty()) {
                ctrl.setWindowOperation(windowOp);
            }
            String windowPath = ctrl.setWindowActor(
                    getInt(nWindowTF.getText(), DEFAULTWORKERS), 
                    windowPathTF.getText(), 
                    remoteWindowCB.isSelected(), 
                    getInt(windowStepTF.getText(), 5), 
                    getInt(windowWidthTF.getText(), 10),
                    getInt(windowHeightTF.getText(), 10));
            windowPathTF.setText(windowPath);
            return true;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this,
                    "Error encountered updating sliding window stage:\n" + e.getMessage(),
                    "Unable To Update Sliding Window Stage",
                    JOptionPane.ERROR_MESSAGE);
            log.error(e.getMessage());
        }
        return false;
    }
    
    /**
     * Handles request to update the ROI Detector stage.
     * @param evt 
     */
    private void udROIBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_udROIBtnActionPerformed
        updateROIDetectionStage();
    }//GEN-LAST:event_udROIBtnActionPerformed

    /**
     * Updates the ROI Detector stage with current settings.
     * @return true if successful false otherwise
     */
    public boolean updateROIDetectionStage() {
        try {
            String roiCommandLine = roiFileTF.getText();
            String[] args = null;
            String[] tokens = roiCommandLine.split("\\s");
            String roiApplication = tokens[0];
            if (tokens.length > 1) {
                args = (String[])ArrayUtils.subarray(tokens, 1, tokens.length);
            }
            String appSelection = ((String)roiTypeCB.getSelectedItem()).toLowerCase();
            ROITYPE appType = getROITYPE(appSelection);
            setROI(appType, roiApplication, args);
            //updateROIDetectionStage();
            String roiPath = ctrl.setROIFinderActor(
                    getInt(nROITF.getText(), DEFAULTWORKERS), 
                    roiPathTF.getText(), 
                    remoteROICB.isSelected());
            roiPathTF.setText(roiPath);
            return true;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this,
                    "Error encountered updating Region Of Interest detection stage:\n" + e.getMessage(),
                    "Unable To Update ROI Detection Stage",
                    JOptionPane.ERROR_MESSAGE);
            log.error(e.getMessage());
        }
        return false;
    }
    
    /**
     * Handles request to update the Reporting stage.
     * @param evt 
     */
    private void udRptBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_udRptBtnActionPerformed
        updateReportingStage();
    }//GEN-LAST:event_udRptBtnActionPerformed

    /**
     * Handles request to show About Dialog
     * @param evt 
     */
    private void aboutMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aboutMenuItemActionPerformed
        AboutDialog aboutDlg = new AboutDialog(this, true);
        int x = getX() + getWidth()/2 - aboutDlg.getWidth()/2;
        int y = getY() + getHeight()/2;
        aboutDlg.setLocation(x, y);
        aboutDlg.setVisible(true);
    }//GEN-LAST:event_aboutMenuItemActionPerformed

    /**
     * Handles request to remove one or more source files from processing.
     * @param evt 
     */
    private void delIngestionSrcBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_delIngestionSrcBtnActionPerformed
        int[] selectedSources = ingestionSrcLB.getSelectedIndices();
        if (selectedSources != null && selectedSources.length > 0) {
            int retval = JOptionPane.showConfirmDialog(this, 
                    "Remove selected file(s) from processing?\nNo files will be deleted.", 
                    "Remove Selected Data Sources?", 
                    JOptionPane.YES_NO_OPTION);
            if (retval == JOptionPane.YES_OPTION) {
                for (int idx: selectedSources) {
                    ingestionSrcModel.remove(idx);
                }
            }
        } else {
            JOptionPane.showMessageDialog(this,
                    "Please select one or more files to remove.",
                    "No Input Files Selected",
                    JOptionPane.ERROR_MESSAGE
            );
        }
    }//GEN-LAST:event_delIngestionSrcBtnActionPerformed

    private void getResultsBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_getResultsBtnActionPerformed
        getResults();
    }//GEN-LAST:event_getResultsBtnActionPerformed

    private void clrResultsBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clrResultsBtnActionPerformed
        ctrl.clearResults();
    }//GEN-LAST:event_clrResultsBtnActionPerformed

    private void clrReportsBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clrReportsBtnActionPerformed
        ctrl.clearReports();
    }//GEN-LAST:event_clrReportsBtnActionPerformed

    /**
     * Handles request to configure the preprocessing stage i.e. DatasetOperation
     * performed on entire Dataset.
     * @param evt 
     */
    private void udPreProcBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_udPreProcBtnActionPerformed
        updatePreProcessingStage();
    }//GEN-LAST:event_udPreProcBtnActionPerformed

    /**
     * Handles request to configure a pre-processing DatasetOperation.
     * @param evt 
     */
    private void configOpBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_configOpBtnActionPerformed
        ctrl.setPreProcessingOperation((String)preprocOpCB.getSelectedItem());
    }//GEN-LAST:event_configOpBtnActionPerformed

    private void emphysicMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_emphysicMenuItemActionPerformed
        AboutEmphysicDialog dlg = new AboutEmphysicDialog(this, true);
        dlg.setVisible(true);
    }//GEN-LAST:event_emphysicMenuItemActionPerformed

    private void licenseMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_licenseMenuItemActionPerformed
        LicenseDialog dlg = new LicenseDialog(this, true);
        dlg.setVisible(true);
    }//GEN-LAST:event_licenseMenuItemActionPerformed

    private void opendocsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opendocsMenuItemActionPerformed
        String docUrl = "https://gitlab.com/ccoughlin/myriad-docs/blob/master/docs/desktop.md";
        try {
            if (Desktop.isDesktopSupported()) {
                Desktop.getDesktop().browse(new URI(docUrl));
            } else {
                log.warn("Desktop not supported, skipping request to load emphysic.com");
                JOptionPane.showMessageDialog(this,
                        "This feature is not supported on this system.  Please visit\n"
                        + docUrl,
                        "Feature Not Supported",
                        JOptionPane.WARNING_MESSAGE
                );
            }
        } catch (URISyntaxException | IOException ex) {
            log.warn("Unable to launch web site: " + ex.getMessage());
            JOptionPane.showMessageDialog(this,
                    "Unable to launch web browser, please visit\n"
                    + docUrl,
                    "Unable To Start Browser",
                    JOptionPane.WARNING_MESSAGE
            );
        }
    }//GEN-LAST:event_opendocsMenuItemActionPerformed

    private void startLogCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startLogCBActionPerformed
        ctrl.setProperty("desktop.log.show", String.valueOf(startLogCB.isSelected()));        
    }//GEN-LAST:event_startLogCBActionPerformed

    private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMenuItemActionPerformed
        dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }//GEN-LAST:event_exitMenuItemActionPerformed

    /**
     * Handles request to export ROI results
     * @param evt 
     */
    private void saveReportsBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveReportsBtnActionPerformed
        JFileChooser jfc = new JFileChooser();
        jfc.setDialogType(JFileChooser.SAVE_DIALOG);
        jfc.setDialogTitle("Please specify an output filename.");
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "Text Files", "txt");
        jfc.addChoosableFileFilter(filter);
        jfc.setFileFilter(filter);
        try {
            if (jfc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
                ctrl.saveResults(jfc.getSelectedFile());
                JOptionPane.showMessageDialog(this,
                        "ROI results saved to\n" + jfc.getSelectedFile(),
                        "Results Saved Successfully",
                        JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (IOException e) {
            JOptionPane.showMessageDialog(this,
                    "Error saving current results: " + e.getMessage(),
                    "Unable To Save Results", JOptionPane.ERROR_MESSAGE);
            log.warn("Unable to save results: " + e.getMessage());
        }
    }//GEN-LAST:event_saveReportsBtnActionPerformed
    
    /**
     * Handles request to compile ROI for each input and export.
     * @param evt 
     */
    private void saveResultsBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveResultsBtnActionPerformed
        JFileChooser jfc = new JFileChooser();
        jfc.setDialogType(JFileChooser.DIRECTORIES_ONLY);
        jfc.setDialogTitle("Please specify a destination folder.");
        try {
            if (jfc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
                Object[] possibleValues = ROIResultGenerator.REPTYPE.values();
                File destFolder = jfc.getSelectedFile();
                if (!destFolder.isDirectory()) {
                    destFolder = destFolder.getParentFile();
                }
                ROIResultGenerator.REPTYPE selectedValue = (ROIResultGenerator.REPTYPE) JOptionPane.showInputDialog(
                        (JFrame) SwingUtilities.getWindowAncestor(resultsLB),
                        "Please choose a method for displaying the ROI.",
                        "Choose ROI Display",
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        possibleValues,
                        possibleValues[0]);
                if (selectedValue != null) {
                    ctrl.saveCompiledResults(destFolder, selectedValue);
                }
                JOptionPane.showMessageDialog(this,
                        "ROI results saved to\n" + destFolder,
                        "Results Saved Successfully",
                        JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (IOException e) {
            JOptionPane.showMessageDialog(this,
                    "Error saving current results: " + e.getMessage(),
                    "Unable To Save Results", JOptionPane.ERROR_MESSAGE);
            log.warn("Unable to save results: " + e.getMessage());
        }       
    }//GEN-LAST:event_saveResultsBtnActionPerformed

    /**
     * Handles request to edit Properties.
     * @param evt 
     */
    private void mnuPropsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuPropsActionPerformed
        PropertiesDialog dlg = new PropertiesDialog(this, false);
        dlg.setProperties(ctrl.getProperties());
        dlg.setVisible(true);
        dlg.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                if (dlg.accepted()) {
                    System.out.println(dlg.getProperties());
                    ctrl.saveProperties(dlg.getProperties());
                }
            }
        });        
    }//GEN-LAST:event_mnuPropsActionPerformed
    
    /**
     * Shows the Log Window and updates the default position in Properties.
     */
    private void showLogWindow(boolean visible) {        
        String posProp = ctrl.getProperty("desktop.log.pos");
        String dimsProp = ctrl.getProperty("desktop.log.dims");        
        int x;
        int y;
        int w;
        int h;        
        if (visible) {
            if (posProp != null && !posProp.isEmpty()) {
                String[] toks = posProp.split(",");
                if (toks.length == 2) {
                    x = Integer.valueOf(toks[0]);
                    y = Integer.valueOf(toks[1]);
                    logWindow.setLocation(x, y);
                }
            }
            if (dimsProp != null && !dimsProp.isEmpty()) {
                String[] toks = dimsProp.split(",");
                if (toks.length == 2) {
                    w = Integer.valueOf(toks[0]);
                    h = Integer.valueOf(toks[1]);
                    logWindow.setSize(w, h);
                }
            }            
        } else {
            x = logWindow.getLocation().x;
            y = logWindow.getLocation().y;
            ctrl.setProperty("desktop.log.pos", Integer.toString(x) + "," + Integer.toString(y));
            w = logWindow.getWidth();
            h = logWindow.getHeight();
            ctrl.setProperty("desktop.log.dims", Integer.toString(w) + "," + Integer.toString(h));
        }
        logWindow.setVisible(visible);
    }
    
    private void getResults() {
        ctrl.getResults();
    }
    
    /**
     * Updates the Reporting Stage with current settings.
     * @return true if successful false otherwise
     */
    public boolean updateReportingStage() {
        try {
            String reportingPath = ctrl.setReporterActor(
                    getInt(nReportersTF.getText(), DEFAULTWORKERS),
                    reporterPathTF.getText(),
                    remoteReporterCB.isSelected());
            reporterPathTF.setText(reportingPath);
            return true;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this,
                    "Error encountered updating reporting stage:\n" + e.getMessage(),
                    "Unable To Update Reporting Stage",
                    JOptionPane.ERROR_MESSAGE);
            log.error(e.getMessage());
        }
        return false;
    }
    
    /**
     * Handles request to set a path to an interpreter e.g. MATLAB or Python.
     * @return absolute path of interpreter executable or null if not set
     */
    public String getInterpreterPath() {
        String p = null;
        JFileChooser fc = new JFileChooser();
        fc.setDialogTitle("Please select an executable.");
        if (fc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            p = fc.getSelectedFile().getAbsolutePath();
        }
        return p;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            String laf = javax.swing.UIManager.getSystemLookAndFeelClassName();
            if (laf.equals(javax.swing.UIManager.getCrossPlatformLookAndFeelClassName())) {
                /*
                 * System LAF came back as Metal - usually happens when running any window
                 * manager other than GNOME under Linux, FreeBSD, etc.  Try Nimbus first as
                 * a slightly better option.
                 */
                laf = "javax.swing.plaf.nimbus.NimbusLookAndFeel";
            }
            javax.swing.UIManager.setLookAndFeel(laf);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MyriadDesktop.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        // Enable Hardware Acceleration
        System.setProperty("sun.java2d.opengl", "true");
        // Use system anti-aliasing settings
        System.setProperty("awt.useSystemAAFontSettings", "on");
        System.setProperty("swing.aatext", "true");
        String myriadHostName = System.getProperty("MYRIAD_NAME");
        if (myriadHostName == null || myriadHostName.length() == 0) {
            try {
                System.setProperty("MYRIAD_NAME", 
                        InetAddress.getLocalHost().getHostAddress());
            } catch (UnknownHostException e) {
                log.warn("Unable to get local host name, error was: " + e.getMessage());
                System.setProperty("MYRIAD_NAME", "localhost");
            }
        }
        log.info("Setting Myriad device name to " + System.getProperty("MYRIAD_NAME"));
        // Set the port to listen for remote connections
        String myriadPort = System.getProperty("MYRIAD_PORT");
        if (myriadPort == null || myriadPort.length() == 0) {
            // Set to a random port around 2550
            Random random = new Random();
            int port = 2550 + random.nextInt(10);
            System.setProperty("MYRIAD_PORT", Integer.toString(port));
        }
        log.info("Setting Myriad port to " + System.getProperty("MYRIAD_PORT"));
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new MyriadDesktop().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem aboutMenuItem;
    private javax.swing.JButton addIngestionSrcBtn;
    private javax.swing.JButton chooseROIBtn;
    private javax.swing.JButton clrReportsBtn;
    private javax.swing.JButton clrResultsBtn;
    private javax.swing.JButton configOpBtn;
    private javax.swing.JButton configWinOpBtn;
    private javax.swing.JButton delIngestionSrcBtn;
    private javax.swing.JMenuItem emphysicMenuItem;
    private javax.swing.JMenuItem exitMenuItem;
    private javax.swing.JMenu fileMnu;
    private javax.swing.JButton getResultsBtn;
    private javax.swing.JMenu helpMnu;
    private javax.swing.JPanel ingestionPanel;
    private javax.swing.JTextField ingestionPathTF;
    private javax.swing.JList<String> ingestionSrcLB;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JMenuItem licenseMenuItem;
    private javax.swing.JMenuItem mnuProps;
    private javax.swing.JTextField nIngestionTF;
    private javax.swing.JTextField nPreProcTF;
    private javax.swing.JTextField nPyramidTF;
    private javax.swing.JTextField nROITF;
    private javax.swing.JTextField nReportersTF;
    private javax.swing.JTextField nWindowTF;
    private javax.swing.JCheckBox normDataCB;
    private javax.swing.JMenuItem opendocsMenuItem;
    private javax.swing.JComboBox<String> preprocOpCB;
    private javax.swing.JPanel preprocPanel;
    private javax.swing.JTextField preprocPathTF;
    private javax.swing.JTextField pyramidCutoffTF;
    private javax.swing.JPanel pyramidPanel;
    private javax.swing.JTextField pyramidPathTF;
    private javax.swing.JTextField pyramidRadiusTF;
    private javax.swing.JTextField pyramidScalingTF;
    private javax.swing.JCheckBox remoteIngestionCB;
    private javax.swing.JCheckBox remotePreProcCB;
    private javax.swing.JCheckBox remotePyramidCB;
    private javax.swing.JCheckBox remoteROICB;
    private javax.swing.JCheckBox remoteReporterCB;
    private javax.swing.JCheckBox remoteWindowCB;
    private javax.swing.JTextField reporterPathTF;
    private javax.swing.JPanel reportingPanel;
    private javax.swing.JList<String> resultsLB;
    private javax.swing.JPanel resultsPane;
    private javax.swing.JTabbedPane resultsPanel;
    private javax.swing.JTextField roiFileTF;
    private javax.swing.JList<ROI> roiLB;
    private javax.swing.JPanel roiPanel;
    private javax.swing.JTextField roiPathTF;
    private javax.swing.JComboBox<String> roiTypeCB;
    private javax.swing.JButton runMyriadButton;
    private javax.swing.JButton saveReportsBtn;
    private javax.swing.JButton saveResultsBtn;
    private javax.swing.JMenuItem showLogMenuItem;
    private javax.swing.JPanel slidingWindowPanel;
    private javax.swing.JCheckBoxMenuItem startLogCB;
    private javax.swing.JButton udIngBtn;
    private javax.swing.JButton udPreProcBtn;
    private javax.swing.JButton udPyrBtn;
    private javax.swing.JButton udROIBtn;
    private javax.swing.JButton udRptBtn;
    private javax.swing.JButton udWinBtn;
    private javax.swing.JButton updatePipelineBtn;
    private javax.swing.JMenu viewMnu;
    private javax.swing.JTextField windowHeightTF;
    private javax.swing.JComboBox<String> windowOpCB;
    private javax.swing.JTextField windowPathTF;
    private javax.swing.JTextField windowStepTF;
    private javax.swing.JTextField windowWidthTF;
    // End of variables declaration//GEN-END:variables

    private MouseListener MouseAdapter() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
