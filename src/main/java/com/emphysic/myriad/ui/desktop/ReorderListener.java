/*
 * com.emphysic.myriad.ui.desktop.ReorderListener
 *
 * Copyright (c) 2017 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.emphysic.myriad.ui.desktop;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.DefaultListModel;
import javax.swing.JList;

/**
 * ReorderListener - drag n' drop reordering of items in a Swing JList
 * @author chris
 */
class ReorderListener extends MouseAdapter {
   /**
    * List to monitor
    */
   private JList list;
   /**
    * Index of element closest to initial mouse button press
    */
   private int pressIndex = 0;
   /**
    * Index of element closest to mouse button release
    */
   private int releaseIndex = 0;

   /**
    * Create a new drag n' drop operation for the specified list
    * @param list JList to manage
    */
   public ReorderListener(JList list) {
      if (!(list.getModel() instanceof DefaultListModel)) {
         throw new IllegalArgumentException("List must have a DefaultListModel");
      }
      this.list = list;
   }

   /**
    * Handles mouse button press event
    * @param e 
    */
   @Override
   public void mousePressed(MouseEvent e) {
      pressIndex = list.locationToIndex(e.getPoint());
   }

   /**
    * Handles mouse button release event
    * @param e 
    */
   @Override
   public void mouseReleased(MouseEvent e) {
      releaseIndex = list.locationToIndex(e.getPoint());
      if (releaseIndex != pressIndex && releaseIndex != -1) {
         reorder();
      }
   }

   /**
    * Handles mouse button drag event
    * @param e 
    */
   @Override
   public void mouseDragged(MouseEvent e) {
      mouseReleased(e);
      pressIndex = releaseIndex;      
   }

   /**
    * Reorders elements
    */
   private void reorder() {
      DefaultListModel model = (DefaultListModel) list.getModel();
      Object dragee = model.elementAt(pressIndex);
      model.removeElementAt(pressIndex);
      model.insertElementAt(dragee, releaseIndex);
   }
}