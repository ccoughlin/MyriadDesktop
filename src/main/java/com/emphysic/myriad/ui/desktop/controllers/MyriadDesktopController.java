/*
 * com.emphysic.myriad.ui.desktop.controllers.MyriadDesktopController
 *
 * Copyright (c) 2017 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.emphysic.myriad.ui.desktop.controllers;

import akka.actor.ActorNotFound;
import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.util.Timeout;
import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.ops.AbsoluteValueOperation;
import com.emphysic.myriad.core.data.ops.BinarizeOperation;
import com.emphysic.myriad.core.data.ops.BoxBlur;
import com.emphysic.myriad.core.data.ops.CannyOperation;
import com.emphysic.myriad.core.data.ops.ConvolutionOperation;
import com.emphysic.myriad.core.data.ops.DatasetOperation;
import com.emphysic.myriad.core.data.ops.DifferenceOfGaussiansOperation;
import com.emphysic.myriad.core.data.ops.GaussianBlur;
import com.emphysic.myriad.core.data.ops.GaussianPyramidOperation;
import com.emphysic.myriad.core.data.ops.HOGOperation;
import com.emphysic.myriad.core.data.ops.NormalizeSignalOperation;
import com.emphysic.myriad.core.data.ops.PrewittOperation;
import com.emphysic.myriad.core.data.ops.ScalingOperation;
import com.emphysic.myriad.core.data.ops.ScharrOperation;
import com.emphysic.myriad.core.data.ops.SobelOperation;
import com.emphysic.myriad.core.data.roi.ExternalROIFinder;
import com.emphysic.myriad.core.data.roi.RESTROIFinder;
import com.emphysic.myriad.core.data.roi.ROI;
import com.emphysic.myriad.core.data.roi.ROIBundle;
import com.emphysic.myriad.core.data.roi.ROIFinder;
import com.emphysic.myriad.core.data.util.FileSniffer;
import com.emphysic.myriad.network.DataIngestorPool;
import com.emphysic.myriad.network.DatasetOperationPool;
import com.emphysic.myriad.network.PyramidActorPool;
import com.emphysic.myriad.network.ROIFinderPool;
import com.emphysic.myriad.network.ReporterActorPool;
import com.emphysic.myriad.network.SlidingWindowPool;
import com.emphysic.myriad.network.messages.FileMessage;
import com.emphysic.myriad.network.messages.ShutdownMessage;
import com.emphysic.myriad.ui.desktop.MyriadDesktop;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import scala.concurrent.Await;


/**
 * MyriadDesktopController - controller for the Swing MyriadDesktop UI
 * @author chris
 */
@Slf4j
public class MyriadDesktopController {
    
    public enum ROITYPE {INTERNAL, EXTERNAL, REST};
    
    /**
     * Name of the desktop application configuration file
     */
    private static String propertiesFileName = "myriad_desktop.properties";
    /**
     * Desktop application configuration
     */
    private Properties props;
    
    /**
     * The Akka system
     */
    private ActorSystem system;
    /**
     * Reference to the Data Ingestion Actor, responsible for reading Datasets
     * from inputs
     */
    private ActorRef dataSource;
    /**
     * Reference to the Gaussian Pyramid Actor, responsible for handling scale
     * in Datasets
     */
    private ActorRef pyramid;
    /**
     * Reference to the Preprocessing Actor - DatasetOperations performed on the
     * global Dataset
     */
    private ActorRef preproc;
    /**
     * Reference to the Sliding Window Actor - slides a small "window" across
     * a larger Dataset to analyze one subset at a time
     */
    private ActorRef window;
    /**
     * Reference to the Region Of Interest (ROI) Actor, which receives a Window
     * and reports whether the subset contains an ROI or not
     */
    private ActorRef roi;
    /**
     * Reference to the Reporting Actor, responsible for receiving reports 
     * about ROIs detected and compiling the results
     */
    private ActorRef reporting;
    /**
     * Reference to a Reporter Listener, which listens to the entire Akka system
     * for ROI messages
     */
    private ActorRef listener;
    /**
     * Akka configuration
     */
    private Config config;
    /**
     * Handle to the user interface
     */
    private MyriadDesktop frame;
    /**
     * DatasetOperation performed on the entire Dataset.
     */
    private DatasetOperation preprocOperation;
    /**
     * DatasetOperation performed on a subset.
     */
    private DatasetOperation  windowOperation;
    /**
     * Region Of Interest (ROI) detector.
     */
    private ROIFinder roiFinder;
    /**
     * Lists input files to search for ROI.
     */
    private final DefaultListModel ingestionSrcModel = new DefaultListModel();
    /**
     * Lists ROI detected.
     */
    private final DefaultListModel<ROI> roiListModel = new DefaultListModel<>();
    
    private final DefaultListModel<String> resultsListModel = new DefaultListModel<>();
    
    /**
     * Currently-supported DatasetOperations available for pre-processing and/or
     * windows.
     */
    private final String[] availableOps = new String[] {
        "None",
        "Absolute Value",
        "Binarize",
        "Box Blur",
        "Convolution",
        "Difference of Gaussians",
        "Gaussian Blur",
        "Normalize",
        "Scale",
        "Scharr",
        "Sobel",
        "Prewitt",
        "Canny",
        "Histogram of Oriented Gradients",
    };
    
    /**
     * Currently-supported ROI Finders.
     * Internal - one of Myriad's built-in ROIFinders
     * External - external application
     */
    private final String[] availableROIFinders = new String[] {
        "Internal",
        "External",
        "MATLAB",
        "Python",
        "REST"
    };
    
    /**
     * Timeout - remote systems must respond within this amount of time
     */
    public static final Timeout TIMEOUT = new Timeout(1000, TimeUnit.MILLISECONDS);
    
    private Map<String, List<ROI>> rois;
    
    /**
     * Creates a new Controller.
     * @param frame UI 
     */
    public MyriadDesktopController (MyriadDesktop frame) {
        this(frame, null);
    }
    
    /**
     * Creates a new Controller.
     * @param frame UI
     * @param propsPath path to properties file - uses default if null / empty
     */
    public MyriadDesktopController(MyriadDesktop frame, String propsPath) {
        this.frame = frame;
        rois = new HashMap<>();
        config = ConfigFactory.load();
        system = ActorSystem.create("Myriad", config);
        listener = system.actorOf(
                Props.create(ListenerActor.class, roiListModel, rois), 
                "Listener");
        if (propsPath != null && !propsPath.isEmpty()) {
            propertiesFileName = propsPath;
        } else {
            log.warn("Invalid properties file path '" + propsPath + "', using default");
        }
        log.info("Using properties file '" + propertiesFileName + "'.");
        getProperties();
    }
    
    /**
     * List of currently-supported DatasetOperations available for pre-processing
     * and/or windows.
     * @return list of operation names
     */
    public String[] getAvailableOperations() {
        return availableOps;
    }
    
    /**
     * List of currently-supported Region Of Interest finders.
     * @return list of ROIFinder types
     */
    public String[] getAvailableROIFinders() {
        return availableROIFinders;
    }
    
    /**
     * Returns handle to the current list of Regions Of Interest (ROI)
     * @return current list
     */
    public DefaultListModel<ROI> getROIListModel() {
        return roiListModel;
    }
    
    public DefaultListModel<String> getResultsListModel() {
        return resultsListModel;
    }
    
    /**
     * Configures a DatasetOperation
     * @param opName name of operation
     * @return configured DatasetOperation or null if an error occurred.
     */
    public DatasetOperation getOperation(String opName) {
        DatasetOperation op = null;
        switch (opName.toLowerCase()) {
            case "absolute value":
                op = new AbsoluteValueOperation();
                break;
            case "binarize":
                String t = JOptionPane.showInputDialog(frame,
                        "Enter a binarization threshold.",
                        "Configure Binarize Operation",
                        JOptionPane.OK_OPTION);
                try {
                    double threshold = Double.parseDouble(t);
                    op = new BinarizeOperation(threshold);
                } catch (NumberFormatException nfe) {
                    JOptionPane.showMessageDialog(frame, 
                            "Please enter a valid number for binarization threshold.",
                            "Invalid Threshold Specified",
                            JOptionPane.ERROR_MESSAGE);
                }
                break;
            case "canny":
            case "gaussian blur":
            case "box blur":
                String r = JOptionPane.showInputDialog(frame,
                        "Enter a blur radius.",
                        "Configure Blur Operation",
                        JOptionPane.OK_OPTION);
                try {
                    int radius = Integer.parseInt(r);
                    if (opName.toLowerCase().contains("gaussian")) {
                        op = new GaussianBlur(radius);
                    } else if (opName.toLowerCase().contains("gaussian")) {
                        op = new BoxBlur(radius);
                    } else if (opName.toLowerCase().contains("canny")) {
                        op = new CannyOperation(radius);
                    }
                } catch (NumberFormatException nfe) {
                    JOptionPane.showMessageDialog(frame,
                            "Please enter a valid number for blur radius.",
                            "Invalid Radius Specified",
                            JOptionPane.ERROR_MESSAGE);
                }
                break;
            case "convolution":
                String msg = "Enter a 2D convolution kernel separating columns with\n"
                        + "spaces or commas and rows by semi-colons e.g.\n" 
                        + "e.g. 1 2 3;4 5 6;7 8 9";
                String rawconv = JOptionPane.showInputDialog(frame,
                        msg,
                        "Configure Convolution Operation",
                        JOptionPane.OK_OPTION);
                try {
                    String[] rows = rawconv.split(";");
                    int colCount = rows[0].split("\\s*(,|\\s)\\s*").length;
                    double[][] kernel = new double[rows.length][colCount];
                    for (int i=0; i<rows.length; i++) {
                        String[] els = rows[i].split("\\s*(,|\\s)\\s*");
                        for (int j=0; j<colCount; j++) {
                            kernel[i][j] = Double.parseDouble(els[j]);
                        }
                    }
                    op = new ConvolutionOperation(kernel);
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(frame,
                            "Unable to create kernel:\n" + e.getLocalizedMessage(),
                            "Invalid Kernel Specified",
                            JOptionPane.ERROR_MESSAGE);
                }
                break;
            case "difference of gaussians":
                String rawdog = JOptionPane.showInputDialog(frame,
                        "Enter two blur radii separated by a space or comma e.g. '1 2' or '3,4'.",
                        "Configure Difference of Gaussians Operation",
                        JOptionPane.OK_OPTION);
                try {
                    int r1;
                    int r2;
                    String[] toks = rawdog.split("\\s*(,|\\s)\\s*");
                    r1 = Integer.parseInt(toks[0]);
                    r2 = Integer.parseInt(toks[1]);
                    op = new DifferenceOfGaussiansOperation(r1, r2);
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(frame,
                            "Please enter valid numbers for blur radius.",
                            "Invalid Radius Specified",
                            JOptionPane.ERROR_MESSAGE);
                }
                break;
            case "normalize":
                op = new NormalizeSignalOperation();
                break;
            case "scale":
                op = new ScalingOperation();
                break;
            case "scharr":
                op = new ScharrOperation();
                break;
            case "sobel":
                op = new SobelOperation();
                break;
            case "prewitt":
                op = new PrewittOperation();
                break;
            case "histogram of oriented gradients":
                String rawhog = JOptionPane.showInputDialog(frame,
                        "Enter a cell radius and a block size separated by a space or comma e.g. '1 2' or '1,2'.",
                        "Configure HOG Operation",
                        JOptionPane.OK_OPTION);
                try {
                    int cellRadius;
                    int blockSize;
                    String[] toks = rawhog.split("\\s*(,|\\s)\\s*");
                    cellRadius = Integer.parseInt(toks[0]);
                    blockSize = Integer.parseInt(toks[1]);
                    op = new HOGOperation(cellRadius, blockSize, 9);
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(frame,
                            "Please enter valid numbers.",
                            "Invalid Numbers Specified",
                            JOptionPane.ERROR_MESSAGE);
                }
                break;
            case "none":
            default:
                break;
        }
        return op;
    }
    
    /**
     * Sets and configures the pre-processing operation i.e. the DatasetOperation
     * performed on the complete Dataset.
     * @param opName name of operation
     */
    public void setPreProcessingOperation(String opName) {
        preprocOperation = getOperation(opName);
    }
    
    /**
     * Sets and configures the window operation i.e. the DatasetOperation 
     * performed on each window during the sliding window operation.
     * @param opName name of operation
     */
    public void setWindowOperation(String opName) {
        windowOperation = getOperation(opName);
    }
    
    /**
     * Finds and sets the Region Of Interest (ROI) Finder
     * @param appType type of ROI finder
     * @param roiApp path to the ROI finder
     * @param args command line arguments for the ROI finder
     * @return absolute path to the ROIFinder plus additional args if any for external ROIFinders
     */
    public String getROIFinder(ROITYPE appType, String roiApp, String... args) {
        roiFinder = null;
        String app = null;
        switch (appType) {
            case INTERNAL:
                File modelFile = new File(roiApp);
                ROIBundle bundle = new ROIBundle();
                try {
                    bundle.load(modelFile);
                    roiFinder = bundle;
                    // If the model was created with Trainer set normalization accordingly
                    if (bundle.getMetadataEntry("normalized_samples") != null) {
                        frame.normalizeData(
                                Boolean.parseBoolean(bundle.getMetadataEntry("normalized_samples"))
                        );
                    }
                    log.info("Successfully read " + modelFile);
                } catch (IOException e) {
                    log.info("Couldn't read " + modelFile + ": " + e.getMessage());
                }
                app = modelFile.toString();
                break;
            case EXTERNAL:
                List<String> arguments = null;
                if (args != null && args.length > 0) {
                    arguments = Arrays.asList(args);
                }
                roiFinder = ExternalROIFinder.buildROIFinder(
                        roiApp, 
                        arguments, 
                        new File(roiApp).getParentFile());
                StringBuilder sb = new StringBuilder(roiApp);
                if (arguments != null) {
                    for (String arg: arguments) {
                        sb.append(" ").append(arg);
                    }
                }
                app = sb.toString();
                break;
            case REST:
                roiFinder = new RESTROIFinder(roiApp);
                if (args != null && args.length == 2) {
                    ((RESTROIFinder)roiFinder).setUsername(args[0]);
                    ((RESTROIFinder)roiFinder).setPassword(args[1]);
                }
                app = ((RESTROIFinder)roiFinder).getUrl();
                break;
            default:
                break;
        }
        return app;
    }
    
    /**
     * Connects each stage in the processing pipeline - output from one is
     * sent to the next in line.
     */
    public void start() {
        ActorRef[] pipeline = {
          dataSource,
          pyramid,
          preproc,
          window,
          roi,
          reporting
        };
        if (Arrays.stream(pipeline).filter(x -> x != null).toArray().length == 0) {
            JOptionPane.showMessageDialog(frame,
                    "Couldn't connect processing pipeline - please configure one or more stages.",
                    "No Stages Configured",
                    JOptionPane.ERROR_MESSAGE
            );
            return;
        }
        for (int i=0; i<pipeline.length - 1; i++) {
            ActorRef stage = pipeline[i];
            if (stage != null) {
                for (int j=i + 1; j<pipeline.length; j++) {
                    ActorRef next = pipeline[j];
                    if (next != null) {
                        log.info("Linking " + stage + " to " + next);
                        stage.tell(next, system.guardian());
                        break;
                    }
                }
            }
        }
    }
    
    /**
     * Generates a new Actor ID based on a basename and the current time.
     * @param base basename of the Actor
     * @return name of the form base_[Current time in milliseconds]
     */
    public String genID(String base) {
        return base + "_" + new Date().getTime();
    }
    
    /**
     * Returns an Akka ActorRef from a (possibly remote) machine
     * @param remotePath path to the Actor
     * @return Optional ActorRef
     * @throws Exception if an error occurs e.g. timeout
     */
    public Optional<ActorRef> getActorRef(String remotePath) throws Exception {
        try {
            //akka.<protocol>://<actorsystemname>@<hostname>:<port>/<actor path>
            //ActorSelection selection =getContext().actorSelection("akka.tcp://app@10.0.0.1:2552/user/serviceA/worker");
            ActorSelection selection = system.actorSelection(remotePath);            
            scala.concurrent.Future<ActorRef> future = selection.resolveOne(TIMEOUT);
            ActorRef remote = Await.result(future, TIMEOUT.duration());
            return Optional.of(remote);
        } catch (ActorNotFound e) {
            log.info("No reference found for " + remotePath);
            return Optional.empty();
        }
    }
    
    /**
     * Sends the shutdown message to an Akka Actor.
     * @param actor Actor to shut down
     */
    public void sendShutdown(ActorRef actor) {
        log.info("Sending shutdown notice to " + actor);
        actor.tell(new ShutdownMessage(), system.guardian());
    }
    
    /**
     * Sends the shutdown message to an Akka Actor.
     * @param opt Actor to shut down
     */
    public void sendShutdown(Optional<ActorRef> opt) {
        if (opt.isPresent()) {
            sendShutdown(opt.get());
        }
    }
    
    /**
     * Instructs the Actor system to terminate
     */
    public void shutdown() {
        system.terminate();
    }
        
    /**
     * Sets the Data Ingestor Actor to a local pool or to a remote Actor.  If local,
     * shuts down the existing Ingestor (if any) and starts a new one.
     * @param numWorkers number of local workers (ignored for remote)
     * @param actorPath path to Actor - shut down (local) or connect to (remote)
     * @param remote true if Actor is in separate system
     * @param normalize true if data should be normalized
     * @return path to the local/remote Actor
     * @throws Exception if an error occurs
     */
    public String setIngestorActor(int numWorkers, String actorPath, boolean remote, boolean normalize) throws Exception {
        String ingestorPath = null;
        // TODO: Need to only check for existing if actorPath != null && actorPath.length() > 0 -> 
        // otherwise looks at root folder Actor
        //Optional<ActorRef> existing = getActorRef(actorPath);     
        
        if (remote) {
            /*if (existing.isPresent()) {
                system.stop(existing.get());
            }*/
            dataSource = system.actorOf(
                    Props.create(RelayActor.class, actorPath), 
                    genID("Remote_Data_Ingestor"));
        } else {
            /*if (existing.isPresent()) {
                sendShutdown(existing);
            }*/
            dataSource = system.actorOf(
                    Props.create(DataIngestorPool.class,
                            numWorkers,
                            normalize), 
                    genID("Data_Ingestor")
            );
        }
        if (dataSource != null) {
            ingestorPath = dataSource.path().toString();
        }
        log.info("New ingestor: " + dataSource);
        return ingestorPath;
    }
    
    /**
     * Sets the Pyramid Actor to a local pool or a remote Actor.  If local,
     * shuts down the current Pyramid (if any) and creates a new one.
     * @param numWorkers number of workers in local pool (ignored if remote)
     * @param actorPath path to the Actor - shut down (local) or connect to (remote)
     * @param remote true if Actor is remote
     * @param blurRadius blur radius in points (ignored if remote)
     * @param scalingFactor size reduction factor between steps (ignored if remote)
     * @param windowSize pyramid cutoff size (ignored if remote)
     * @return path to the new Actor
     * @throws Exception if an error occurs
     */
    public String setPyramidActor(
            int numWorkers, 
            String actorPath, 
            boolean remote, 
            int blurRadius, 
            int scalingFactor, 
            int windowSize) throws Exception {
        String pyramidPath = null;
        Optional<ActorRef> existing = getActorRef(actorPath);
        /*if (existing.isPresent()) {
            sendShutdown(existing);
        }*/
        if (remote) {
            pyramid = system.actorOf(
                    Props.create(RelayActor.class, actorPath), 
                    genID("Remote_Pyramid"));
        } else {
            pyramid = system.actorOf(
                    Props.create(PyramidActorPool.class,
                            numWorkers, 
                            new GaussianPyramidOperation(scalingFactor, windowSize)), 
                    genID("Pyramid"));
        }
        if (pyramid != null) {
            pyramidPath = pyramid.path().toString();
        }
        log.info("New pyramid: " + pyramid);
        return pyramidPath;
    }
    
    /**
     * Sets the pre-processing Actor to a local pool or a remote Actor.  If local,
     * shuts down the current preprocessor (if any) and creates a new one.
     * @param numWorkers number of workers in the pool (ignored if remote)
     * @param actorPath path to Actor - shut down (local) or connect to (remote)
     * @param remote true if Actor is remote
     * @return path to new Actor
     * @throws Exception if an error occurs
     */
    public String setPreProcessorActor(
            int numWorkers,
            String actorPath,
            boolean remote) throws Exception {
        String preprocPath = null;
        Optional<ActorRef> existing = getActorRef(actorPath);
        /*if (existing.isPresent()) {
            sendShutdown(existing);
        }*/
        if (remote) {
            preproc = system.actorOf(
                    Props.create(RelayActor.class, actorPath), 
                    genID("Remote_Preprocessor"));
        } else {
            if (preprocOperation != null) {
                preproc = system.actorOf(
                        Props.create(DatasetOperationPool.class,
                                numWorkers, 
                                preprocOperation), 
                        genID("Preprocessor"));
            } else {
                // No preprocessing 
                preproc = null;
            }
        }
        if (preproc != null) {
            preprocPath = preproc.path().toString();
        }
        log.info("New preprocessor: " + preproc);
        return preprocPath;
    }
    
    /**
     * Sets the Sliding Window Actor to a local pool or a remote Actor.  If local,
     * shuts down the current Sliding Window (if any) and creates a new one.
     * @param numWorkers number of workers in the pool (ignored if remote)
     * @param actorPath path of Actor - shut down (local) or connect to (remote)
     * @param remote true if remote
     * @param stepSize steps between windows in points (ignored if remote)
     * @param windowWidth width of window (ignored if remote)
     * @param windowHeight height of window (ignored if remote)
     * @return path to new Actor
     * @throws Exception if an error occurs
     */
    public String setWindowActor(
            int numWorkers,
            String actorPath,
            boolean remote,
            int stepSize,
            int windowWidth,
            int windowHeight) throws Exception {
        String windowPath = null;
        Optional<ActorRef> existing = getActorRef(actorPath);
        /*if (existing.isPresent()) {
            sendShutdown(existing);
        }*/
        if (remote) {
            window = system.actorOf(
                    Props.create(RelayActor.class, actorPath), 
                    genID("Remote_Sliding_Window"));
        } else {
            window = system.actorOf(Props.create(SlidingWindowPool.class,
                            numWorkers, 
                            stepSize,
                            windowWidth,
                            windowHeight,
                            windowOperation), 
                    genID("Sliding_Window"));
        }
        if (window != null) {
            windowPath = window.path().toString();
        }
        log.info("New sliding window: " + window);
        return windowPath;
    }
    
    /**
     * Sets the Region Of Interest (ROI) Actor to a local pool or a remote Actor.
     * If local, shuts down the current ROI Actor (if any) and creates a new one.
     * @param numWorkers number of workers in the pool (ignored if remote)
     * @param actorPath path to Actor - shut down (local) or connect to (remote)
     * @param remote true if remote
     * @return path to new Actor
     * @throws Exception if an error occurs
     */
    public String setROIFinderActor(
            int numWorkers,
            String actorPath,
            boolean remote) throws Exception {
        String roiPath = null;
        Optional<ActorRef> existing = getActorRef(actorPath);
        /*if (existing.isPresent()) {
            sendShutdown(existing);
        }*/
        if (remote) {
            roi = system.actorOf(
                    Props.create(RelayActor.class, actorPath), 
                    genID("Remote_ROI_Finder"));
        } else {
            if (roiFinder != null) {
                roi = system.actorOf(
                        Props.create(ROIFinderPool.class,
                                numWorkers, 
                                roiFinder), 
                        genID("ROI_Finder"));
            }
        }
        if (roi != null) {
            roiPath = roi.path().toString();
        }
        log.info("New ROI finder: " + roi);
        return roiPath;
    }
    
    /**
     * Sets the Reporter Actor to a local pool or remote Actor.  If local, shuts
     * down the current Reporter Actor (if any) and creates a new one.
     * @param numWorkers number of workers in the pool (ignored if remote)
     * @param actorPath path to Actor to shut down (local) or connect to (remote)
     * @param remote true if remote
     * @return path to new Actor
     * @throws Exception if an error occurs
     */
    public String setReporterActor(
            int numWorkers,
            String actorPath,
            boolean remote) throws Exception {
        String reporterPath = null;
        Optional<ActorRef> existing = getActorRef(actorPath);
        /*if (existing.isPresent()) {
            sendShutdown(existing);
        }*/
        if (remote) {
            reporting = system.actorOf(
                    Props.create(RelayActor.class, actorPath), 
                    genID("Remote_Reporting"));
        } else {
            reporting = system.actorOf(
                    Props.create(ReporterActorPool.class,
                            numWorkers), 
                    genID("Reporting"));
        }
        if (reporting != null) {
            reporterPath = reporting.path().toString();
            reporting.tell(listener, system.guardian());
        }
        log.info("New reporter: " + reporting);
        return reporterPath;
    }
    
    public void ingestInputFile(String inputFileName) {
        File f = new File(inputFileName);
        if (f.canRead() && dataSource != null) {
            log.info("Ingesting " + f);
            dataSource.tell(new FileMessage(f), system.guardian());
        } else {
            log.warn("Unable to read " + f);
        }
    }
    
    /**
     * Retrieves the Properties for the desktop application from the properties
     * file.  If the file doesn't exist, it is created.
     * @return properties
     */
    public Properties getProperties() {
        File propFile = new File(propertiesFileName);
        props = new Properties();
        if (propFile.canRead()) { 
            try {
               FileInputStream fis = new FileInputStream(propFile);
               props.load(fis);
            } catch (IOException ioe) {
                log.warn("Unable to read properties file: " + ioe.getMessage());
            }
        } else {
            saveProperties();
        }
        return props;
    }
    
    /**
     * Replaces the current Properties with a new instance and saves.
     * @param p new Properties
     * @return true if save was successful
     */
    public boolean saveProperties(Properties p) {
        props = p;
        return saveProperties();
    }
    
    /**
     * Saves the current properties.
     * @return true if successful, false otherwise
     */
    public boolean saveProperties() {
        File propFile = new File(propertiesFileName);
        try {
            props.store(new FileOutputStream(propFile), 
                "Properties for the Myriad Desktop Application");
            return true;
        } catch (IOException ioe) {
            log.warn("Unable to store properties: " + ioe.getMessage());
        }
        return false;
    }
    
    /**
     * Retrieve a property.
     * @param propertyName name of the property
     * @return value of the property, or null if not found.
     */
    public String getProperty(String propertyName) {
        return props.getProperty(propertyName);
    }
    
    /**
     * Sets a property.
     * @param propertyName name of the property
     * @param propertyValue value of the property
     */
    public void setProperty(String propertyName, String propertyValue) {
        props.setProperty(propertyName, propertyValue);
    }
    
    public Map<String, List<ROI>> getResults() {
        rois.keySet().stream().forEach((src) -> {
            resultsListModel.addElement(src);
        });
        return rois;
    }
    
    /**
     * Saves the current list of ROI as a text file.
     * @param out output file
     * @throws IOException if an I/O error occurs
     */
    public void saveResults(File out) throws IOException {
        for (String source: rois.keySet()) {
            for (ROI r: rois.get(source)) {
                FileUtils.writeStringToFile(out, r.toString() + "\n", "UTF-8", true);
            }
        }
    }
    
    /**
     * Compiles the ROI for each input file and exports the resultant dataset as text files.
     * @param fldr
     * @param representation
     * @throws IOException 
     */
    public void saveCompiledResults(File fldr, ROIResultGenerator.REPTYPE representation) throws IOException {
        for (String source: rois.keySet()) {
            Dataset d = getResultDataset(source, representation);
            if (d != null) {
                String base = FilenameUtils.getBaseName(source) + ".txt";
                File out = new File(fldr, base);
                d.write(out);
            }
        }
    }
    
    public List<ROI> getResults(String src) {
        return new ArrayList<>(rois.get(src));
    }   
        
    public Dataset getResultDataset(String src, ROIResultGenerator.REPTYPE representation) {
        Dataset res = null;
        File srcFile = new File(src);
        if (srcFile.canRead()) {
            try {
                List<ROI> regions = getResults(src);
                ROIResultGenerator resultGenerator = new ROIResultGenerator();
                res = resultGenerator.generate(FileSniffer.read(srcFile, true), regions, representation);
            } catch (IOException ioe) {
                log.error("Couldn't read source file " + srcFile + " :" + ioe.getMessage());
            }
        }     
        return res;
    }
    
    public void clearResults() {
        resultsListModel.clear();
        rois.clear();
        clearReports();
    }
    
    public void clearReports() {
        roiListModel.clear();
    }

    /**
     * @return the propertiesFileName
     */
    public static String getPropertiesFileName() {
        return propertiesFileName;
    }

    /**
     * @param aPropertiesFileName the propertiesFileName to set
     */
    public static void setPropertiesFileName(String aPropertiesFileName) {
        propertiesFileName = aPropertiesFileName;
    }
}
