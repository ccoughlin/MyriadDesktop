/*
 * com.emphysic.myriad.ui.desktop.DatasetPanel
 *
 * Copyright (c) 2017 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.emphysic.myriad.ui.desktop;

import com.emphysic.myriad.core.data.io.Dataset;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ItemEvent;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import javax.swing.JOptionPane;
import lombok.extern.slf4j.Slf4j;
import smile.plot.Heatmap;
import smile.plot.Palette;
import smile.plot.PlotCanvas;
import smile.plot.ScatterPlot;

/**
 * DatasetPanel - Swing JPanel for plotting Datasets
 * @author chris
 */
@Slf4j
public class DatasetPanel extends javax.swing.JPanel {
    private double[][] currentData;
    /**
     * Creates new form DatasetPanel
     */
    public DatasetPanel() {
        initComponents();
        plotCB.addItemListener((ItemEvent e) -> {
            String selectedPlot = (String)plotCB.getSelectedItem();
            switch (selectedPlot) {
                case "Scatter":
                    alphaTF.setEnabled(false);
                    ncolorsTF.setEnabled(false);
                    paletteCB.setEnabled(false);
                    break;
                default:
                    alphaTF.setEnabled(true);
                    ncolorsTF.setEnabled(true);
                    paletteCB.setEnabled(true);
                    break;
            }
        });
    }

    /**
     * Plots Dataset
     * @param data Dataset to plot
     */
    public void showDataset(Dataset data) {
        if (data != null) {
            currentData = new double[data.getWidth()][data.getHeight()];
            for (int i=0; i<data.getWidth(); i++) {
                for (int j=0; j<data.getHeight(); j++) {
                    currentData[i][j] = data.get(i, j);
                }
            }
            plot();
        }
    }
    
    /**
     * Plots the current Dataset.
     */
    public void plot() {
        if (plotPanel.getComponentCount() > 0) {
            plotPanel.remove(plotPanel.getComponent(0));
        }
        PlotCanvas canvas = null;
        String selectedPlot = (String)plotCB.getSelectedItem();
        Color[] palette = getPalette();
        switch (selectedPlot) {
            case "Heatmap":
                canvas = Heatmap.plot(currentData, palette);
                break;
            /*
                Leave other plots disabled for now - horrible performance on large datasets,
                likely a Swing thing
            case "Contour":
                canvas = Contour.plot(currentData);
                break;
            case "Surface":
                canvas = Surface.plot(currentData, palette);
                break;
            case "Wireframe":
                // TODO: implement wireframe
                canvas = Surface.plot(currentData, palette);
                break;
            */
            case "Scatter":
                try {
                    canvas = ScatterPlot.plot(currentData);
                } catch (IllegalArgumentException iae) {
                    // Data isn't X-Y
                    // TODO - handle some cases e.g. 1D data if req'd, offer to reshape, etc.
                    JOptionPane.showMessageDialog(this, 
                            "Current data is " + currentData.length + "x" + currentData[0].length + ", couldn't produce scatter plot.", 
                            "Unable To Plot Dataset", 
                            JOptionPane.ERROR_MESSAGE);
                    log.error("Couldn't create scatter plot - incorrect dimensions: " + iae.getMessage());
                }
                break;
            default:
                log.error("Invalid type of plot specified: " + selectedPlot);
                break;
        }
        if (canvas != null) {
            canvas.validate();
            plotPanel.setLayout(new BorderLayout());
            plotPanel.add(canvas, BorderLayout.CENTER);
            plotPanel.validate();
        }
    }
    
    /**
     * Retrieves a pre-formatted color palette
     * @return palette
     */
    public Color[] getPalette() {
        try {
            Palette p = new Palette();
            String paletteName = (String)paletteCB.getSelectedItem();
            Method method = p.getClass().getMethod(paletteName, int.class, float.class);
            Color[] palette = (Color[]) method.invoke(p, Integer.parseInt(ncolorsTF.getText()), Float.parseFloat(alphaTF
                    .getText()));
            return palette;
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException | IllegalArgumentException e) {
            // Couldn't find static palette method
            e.printStackTrace();
        }
        // Underlying object threw an exception
        // Method isn't accessible
        return Palette.jet(256, (float) 1.0);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToolBar1 = new javax.swing.JToolBar();
        plotCB = new javax.swing.JComboBox<>();
        paletteCB = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        ncolorsTF = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        alphaTF = new javax.swing.JTextField();
        updatePlotBtn = new javax.swing.JButton();
        plotPanel = new javax.swing.JPanel();

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        plotCB.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Heatmap", "Scatter" }));
        plotCB.setToolTipText("Select Dataset plot type");
        plotCB.setEnabled(false);
        jToolBar1.add(plotCB);

        paletteCB.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "jet", "rainbow", "heat", "redblue", "redgreen", "terrain", "topo" }));
        paletteCB.setToolTipText("Select palette family for heatmap");
        jToolBar1.add(paletteCB);

        jLabel1.setText("Colors");
        jToolBar1.add(jLabel1);

        ncolorsTF.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        ncolorsTF.setText("256");
        ncolorsTF.setToolTipText("Number of colors to use in selected palette");
        jToolBar1.add(ncolorsTF);

        jLabel2.setText("Alpha");
        jToolBar1.add(jLabel2);

        alphaTF.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        alphaTF.setText("0.5");
        alphaTF.setToolTipText("Transparency of heatmap: 0 (transparent) to 1.0 (opaque)");
        jToolBar1.add(alphaTF);

        updatePlotBtn.setText("Update");
        updatePlotBtn.setToolTipText("Updates the plot with current settings");
        updatePlotBtn.setFocusable(false);
        updatePlotBtn.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        updatePlotBtn.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        updatePlotBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updatePlotBtnActionPerformed(evt);
            }
        });
        jToolBar1.add(updatePlotBtn);

        plotPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        javax.swing.GroupLayout plotPanelLayout = new javax.swing.GroupLayout(plotPanel);
        plotPanel.setLayout(plotPanelLayout);
        plotPanelLayout.setHorizontalGroup(
            plotPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        plotPanelLayout.setVerticalGroup(
            plotPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 381, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, 468, Short.MAX_VALUE)
            .addComponent(plotPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(plotPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void updatePlotBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updatePlotBtnActionPerformed
        plot();
    }//GEN-LAST:event_updatePlotBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField alphaTF;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JTextField ncolorsTF;
    private javax.swing.JComboBox<String> paletteCB;
    private javax.swing.JComboBox<String> plotCB;
    private javax.swing.JPanel plotPanel;
    private javax.swing.JButton updatePlotBtn;
    // End of variables declaration//GEN-END:variables
}
